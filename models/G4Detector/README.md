## G4Detector
[G4Detector](https://github.com/OrensteinLab/G4detector) is a multi-kernel convolutional neural networks, aimed
at classifying DNA sequences for having the potential to form G-quadruplex (G4). We run G4Detector in this project to 
compare performance with G4mer, our transformer based model.

## How to run G4Detector
Bash scripts have been prepared to conveniently run G4Detector for binary and multiclass classifications.

### Running G4Detector for binary classification
To run G4Detector for binary classification, use run_g4detector_binary.sh. Update the `TRAIN_DATA` and `TEST_DATA`
variables to the complete paths of train and test data respectively, then run `sh run_g4detector_binary.sh`

### Running G4detector for multiclass classification
Similarly, to run G4Detector for multiclass classification, use run_g4detector_multiclass.sh. 
Run `sh run_g4detector_multiclass.sh` after updating the train and test data variables.
"""
This file takes non g4 sequences, extends to specified lengths, and ensures that it doesn't overlap with g4 sequences
"""

# Coordinates are zero indexed, left inclusive and right exclusive
from utils import utils
import pandas as pd
import os
import random
import math
import sys
from tqdm import tqdm

DATA_DIR = os.path.join("/", "data", "g4mer")


def run_all(genome_ver, final_num_of_non_g4, extended_seq_len, out_dir):
    """
    This function generates negative strands from the specified parameters. Writes non_g4_data.txt
    consisting of sequences and labels of the specified extended_seq_len
    :return:
    """
    num_of_non_g4 = final_num_of_non_g4 + 5000 # 5000 is a buffer since we'll lose some sequences
    num_per_chr = int(math.floor(num_of_non_g4/24)) + 1000 # 1000 is a buffer since we'll lose some sequence

    # read g4 and non g4 sequences
    non_g4_coords_dir = os.path.join(DATA_DIR, "non_g4_coords")
    g4_df = pd.read_csv(os.path.join(non_g4_coords_dir, "g4_coords." + genome_ver + ".bed"), sep='\t', header=None)
    g4_df["orientation"] = "."
    g4_df.columns = ['chr', 'start', 'end', 'orientation']

    # get rg4seeker g4 coordinates
    rg4_df = pd.read_csv(os.path.join(DATA_DIR, "rg4seeker", "g4_sequences.txt"), sep='\t')
    rg4_df = rg4_df[['chr', 'start', 'end', 'strand']]
    rg4_df.columns = ["chr", "start", "end", "orientation"]
    g4_df = pd.concat([g4_df, rg4_df], ignore_index=True).reset_index(drop=True)
    assert len(g4_df.columns) == 4
    non_g4_df = pd.read_csv(os.path.join(non_g4_coords_dir, "non_g4_coords." + genome_ver + ".bed"), sep='\t', header=None)
    # the file is hg19 because we're just extracting the orientation from this file, not the coordinates, so it
    # wasn't converted to hg38
    orientation = pd.read_csv(os.path.join(non_g4_coords_dir, "non_g4_coords_with_orientation.hg19.bed"), sep=' ',
                              header=None).iloc[:, 3]
    non_g4_df['orientation'] = orientation
    non_g4_df.columns = ['chr', 'start', 'end', 'orientation']
    # remove unknown chromosomes
    non_g4_df = non_g4_df[non_g4_df["chr"] != "chr15_KI270850v1_alt"]
    non_g4_df = non_g4_df[non_g4_df["chr"] != "chr17_KI270909v1_alt"]
    non_g4_df = non_g4_df[non_g4_df["chr"] != "chr19_KI270938v1_alt"]
    non_g4_df = non_g4_df[non_g4_df["chr"] != "chr22_KI270879v1_alt"]
    non_g4_df = non_g4_df[non_g4_df["chr"] != "chr1_KI270766v1_alt"]
    non_g4_df = non_g4_df[non_g4_df["chr"] != "chr1_KI270765v1_alt"]
    assert len(non_g4_df.columns) == 4

    # print stats
    print("The number of g4 sequences is", len(g4_df))
    print("The number of non g4 sequences is", len(non_g4_df))

    # get g4 and extended non g4 sequences
    print("Getting G4 and extended non G4 sequences...")
    g4_seq = get_seq_dict(df=g4_df, label="G4", sort=True, sort_by="start")
    non_g4_seq = get_seq_dict(non_g4_df, "nonG4", sort=True, sort_by="start",
                              extend=True, extended_length=extended_seq_len)

    # limit the number of non_g4_seq
    for chr in non_g4_seq:
        non_g4_seq[chr] = non_g4_seq[chr][:num_per_chr]

    utils.get_chr_lengths(version=genome_ver)
    destination_file = os.path.join(DATA_DIR, genome_ver, "chr_lengths.txt")

    max_chr_len = dict()
    with open(destination_file, 'r') as file:
        for line in file:
            chr = line.split("\t")[0]
            length = line.split("\t")[1]
            max_chr_len[chr] = length

    # find non g4 sequences that don't contain g4 sequences in it
    print("Finding non overlapping non G4 sequences...")
    non_overlapping_non_g4 = find_non_overlapping_non_g4(g4_seq, non_g4_seq, max_chr_len,
                                                         non_g4_seq_len=extended_seq_len)
    # perform sanity check
    print("Performing sanity check.....")
    non_overlapping_non_g4 = check_non_overlapping_non_g4(non_overlapping_non_g4, g4_seq,
                                                          extended_seq_len=extended_seq_len)

    # write extended non g4 sequences to file
    print("\nGetting sequences from coordinates...\n")
    non_overlapping_non_g4 = utils.get_sequences_from_coordinates(non_overlapping_non_g4, version=genome_ver,
                                                                  orientation=False, find_g4_motif=False)

    # convert label to 0 and 1 for DNAbert
    non_overlapping_non_g4 = utils.convert_label(non_overlapping_non_g4, neg_string="nonG4", pos_string="G4")
    # collapse the chromosome dictionary into a list of sequence dictionaries
    sequence_list = []
    for chr in non_overlapping_non_g4:
        sequence_list = sequence_list + non_overlapping_non_g4[chr]
    random.Random(4).shuffle(sequence_list)

    curr_seq_list = []
    for sequence in sequence_list:
        curr_seq_list.append({
            "sequence": get_sequence(sequence["sequence"], extended_seq_len),
            "label": sequence["label"]
        })
    # remove duplicates and sequences with N nucleotides
    curr_seq_list = filter_sequence_list(curr_seq_list)
    curr_seq_list = curr_seq_list[:final_num_of_non_g4]
    print("Found", len(curr_seq_list), "unique non g4 sequences after cleaning")
    assert len(curr_seq_list) == final_num_of_non_g4

    # write sequences to file
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    out = os.path.join(out_dir, "non_g4_data.txt")
    with open(out, 'w') as file:
        file.write("sequence\tlabel\n")
        for sequence in curr_seq_list:
            if'N' not in sequence["sequence"]:
                file.write("%s\t%s\n" % (sequence["sequence"], sequence["label"]))


def get_sequence(original_seq, extended_seq_len):
    if 'GGG' in original_seq:
        g_run_ind = original_seq.index('GGG')
        # number of nucleotides from g run to the end of the sequence
        num_nucleotides_end = min(extended_seq_len, len(original_seq) - g_run_ind)
        # number of nucleotides needed to be added to the front of the g run
        num_nucleotides_start = extended_seq_len - num_nucleotides_end
        # start index
        start_ind = g_run_ind - num_nucleotides_start
        if 'GGG' not in original_seq[start_ind:start_ind+extended_seq_len]:
            print('original sequence', original_seq)
            print('calculated sequence', original_seq[start_ind:start_ind+extended_seq_len])
            print('g run index', g_run_ind)
            sys.exit()
        return original_seq[start_ind:start_ind+extended_seq_len]
    return original_seq[:extended_seq_len]


def filter_sequence_list(seq_list):
    # remove sequences containing N nucleotide and duplicated sequence
    ret = []
    sequences_encountered_list = []
    for seq in seq_list:
        if 'N' not in seq["sequence"] and seq['sequence'] not in sequences_encountered_list:
            ret.append(seq)
            sequences_encountered_list.append(seq['sequence'])

    assert len(sequences_encountered_list) == len(ret)

    return ret


def get_seq_dict(df, label, sort=True, sort_by="", extend=False, extended_length=0):
    """
    This function takes a dataframe of G4 or non G4 coordinates and create
    a dictionary of chromosomes and the list of coordinates with other information about each sequences
    :param df: The dataframe containing chromosomes and coordinates
    :param label: Label for each of the sequence, G4 or non G4
    :param sort: if true, sort the list of coordinates per chromosome
    :param sort_by: the key to sort the list of cooridnates by
    :param extend: if true, extend/cut the coordinates to get an extended sequence
    :param extended_length: the final length of the sequence after extending/cutting
    :return: the resulting dictionary with chromosomes as keys and list of dictionaries
    of coordinates as values
    """
    seq_dict = dict()
    for index, row in df.iterrows():
        row_dict = dict()
        chromosome = row["chr"]
        start = row["start"]
        end = row["end"]
        orientation = row["orientation"]
        if chromosome not in seq_dict:
            seq_dict[chromosome] = []
        # extend the sequence by placing the original sequence in the middle
        if extend:
            seq_len = end - start
            to_add = (extended_length - seq_len) / 2
            if seq_len % 2 == 1:
                start_to_add = int(math.floor(to_add))
                end_to_add = int(math.ceil(to_add))
            else:
                start_to_add = int(to_add)
                end_to_add = int(to_add)
            row_dict["start"] = start - start_to_add
            row_dict["end"] = end + end_to_add
            row_dict["original_start"] = start
            row_dict["original_end"] = end
        else:
            row_dict["start"] = start
            row_dict["end"] = end
        row_dict["label"] = label
        row_dict["orientation"] = orientation
        seq_dict[chromosome].append(row_dict)

    if sort:
        for chromosome in seq_dict:
            seq_dict[chromosome].sort(key=lambda x: x[sort_by])

    return seq_dict


def find_non_overlapping_non_g4(g4_seq, non_g4_seq, max_chr_len, non_g4_seq_len=512):
    """
    Returns a dictionary of chromosomes as keys and list seq coordinates dictionaries as values
    :param g4_seq:
    :param non_g4_seq:
    :param max_chr_len:
    :param non_g4_seq_len:
    :return:
    """
    extended_non_g4_seq = {}
    for chromosome in non_g4_seq:
        if chromosome not in extended_non_g4_seq:
            extended_non_g4_seq[chromosome] = []
        combined_seq = g4_seq[chromosome] + non_g4_seq[chromosome]
        combined_seq.sort(key=lambda x: x["start"])
        # loop through the sequences to find non G4 seqs
        non_g4_idx = 0
        max_len = len(combined_seq)
        valid = True
        while non_g4_idx < max_len and valid:
            while combined_seq[non_g4_idx]["label"] != "nonG4":
                non_g4_idx += 1
                if non_g4_idx == max_len:
                    valid = False
                    break
            # if there is no more non g4 sequences in the list, exit the loop
            if not valid:
                break
            # otherwise, extend the sequence
            g4_idx1 = non_g4_idx
            while combined_seq[g4_idx1]["label"] != "G4":
                g4_idx1 -= 1
                if g4_idx1 == -1:
                    break
            g4_idx2 = non_g4_idx
            while combined_seq[g4_idx2]["label"] != "G4":
                g4_idx2 += 1
                if g4_idx2 == max_len:
                    break
            if g4_idx1 == -1:
                g4_coord1 = 0
            else:
                g4_coord1 = combined_seq[g4_idx1]["end"]
            if g4_idx2 == max_len:
                g4_coord2 = max_chr_len[chromosome]
            else:
                g4_coord2 = combined_seq[g4_idx2]["start"]
            # check if non g4 overlaps with g4s
            non_g4_start = int(combined_seq[non_g4_idx]["start"])
            non_g4_end = int(combined_seq[non_g4_idx]["end"])
            g4_left = int(g4_coord1)
            g4_right = int(g4_coord2)
            if non_g4_start < g4_left or non_g4_end > g4_right:
                # if the distance between the two G4s is too small, can't do anything
                if g4_right - g4_left < non_g4_seq_len:
                    break
                # otherwise, shift the non g4 such that it's not overlapping
                # with the original g4 coordinates included
                else:
                    if non_g4_start < g4_right:
                        to_shift = g4_left - non_g4_start
                    elif non_g4_end > g4_left:
                        to_shift = g4_right - non_g4_end
                    new_start = combined_seq[non_g4_idx]["start"] + to_shift
                    new_end = combined_seq[non_g4_idx]["end"] + to_shift
                    original_start = combined_seq[non_g4_idx]["original_start"]
                    original_end = combined_seq[non_g4_idx]["original_end"]
                    if new_start <= original_start < original_end <= new_end:
                        combined_seq[non_g4_idx]["start"] = new_start
                        combined_seq[non_g4_idx]["end"] = new_end
                        extended_non_g4_seq[chromosome].append(combined_seq[non_g4_idx])
            elif g4_left <= non_g4_start < non_g4_end <= g4_right:
                extended_non_g4_seq[chromosome].append(combined_seq[non_g4_idx])
            non_g4_idx += 1

    return extended_non_g4_seq


def check_non_overlapping_non_g4(non_overlapping_non_g4, g4_seq, extended_seq_len=0):
    """
    Sanity check
    Check that original start and end are within the extended sequence,
    or if original sequence is longer, then extended sequence is within the original sequence
    Check that there are no g4 coordinates in the extended sequence
    :param non_overlapping_non_g4:
    :return:
    """
    # check that sequence coordinates are valid
    for chr in non_overlapping_non_g4:
        sequence_list = non_overlapping_non_g4[chr]
        for sequence in sequence_list:
            original_seq_len = sequence["original_end"] - sequence["original_start"]
            # If original sequence length is shorter than extended sequence length
            # then the sequence was extended
            if original_seq_len < extended_seq_len:
                assert sequence["start"] <= sequence["original_start"] <= sequence["original_end"] <= sequence["end"]
            # otherwise, the sequence was cut
            else:
                assert sequence["original_start"] <= sequence["start"] <= sequence["end"] <= sequence["original_end"]

    # check that non g4 coordinates do not contain g4 coordinates
    non_g4_intervals = dict()
    g4_intervals = dict()
    for chr in non_overlapping_non_g4:
        if chr not in non_g4_intervals:
            non_g4_intervals[chr] = []
        if chr not in g4_intervals:
            g4_intervals[chr] = []

        non_g4_sequence_list = non_overlapping_non_g4[chr]
        for sequence in non_g4_sequence_list:
            non_g4_intervals[chr].append({
                "interval": pd.Interval(sequence["start"], sequence["end"], closed="left"),
                "orientation": sequence["orientation"]
            })
        g4_sequence_list = g4_seq[chr]
        for sequence in g4_sequence_list:
            g4_intervals[chr].append(
                {
                    "interval": pd.Interval(sequence["start"], sequence["end"], closed="left"),
                    "orientation": sequence["orientation"]
                })
    final_non_overlapping_non_g4 = dict()
    for chr in non_g4_intervals:
        non_g4_interval_list = non_g4_intervals[chr]
        for i in tqdm(range(len(non_g4_interval_list))):
            overlaps = False
            g4_interval_list = g4_intervals[chr]
            for j in range(len(g4_interval_list)):
                # if the interval overlaps
                if non_g4_interval_list[i]["interval"].overlaps(g4_interval_list[j]["interval"]):
                    # and orientations are the same then the sequences overlap
                    if g4_interval_list[i]["orientation"] == '.' or non_g4_interval_list[i]["orientation"] == g4_interval_list[j]["orientation"]:
                        overlaps = True
                        break
            if not overlaps:
                if chr not in final_non_overlapping_non_g4:
                    final_non_overlapping_non_g4[chr] = []
                final_non_overlapping_non_g4[chr].append(non_overlapping_non_g4[chr][i])
    return final_non_overlapping_non_g4
#!/usr/bin/sh

export TRAIN_DATA=/data/g4mer/train_dev/rg4seeker/binary/70n/train.tsv
export TEST_DATA=/data/g4mer/train_dev/rg4seeker/binary/70n/dev.tsv

python g4.py -i $TRAIN_DATA -t $TEST_DATA
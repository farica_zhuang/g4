import pandas as pd
from tqdm import tqdm
import os
from Bio import SeqIO
import gzip
import portion as P

# from data import data_exploration
from collections import defaultdict
from utils import utils
import pathlib


# global variables
DATA_DIR = os.path.join("/", "data", "g4mer")


# -----------------------------Functions to get dinucleotide distributions--------------------------------
def output_all_region_distributions(version='v29'):
    """
    Output file of dinucleotide distributions for each mRNA region: 5'UTR, 3'UTR, CDS, long noncoding
    :param version: version of gencode to get the region distributions of
    :return: N/A
    """
    # create dinucleotide distribution folder
    dinucleotide_dist_data_dir = os.path.join(DATA_DIR, "dinucleotide_distributions", version)

    # if the dinucleotide files exist, skip the process
    if os.path.exists(dinucleotide_dist_data_dir):
        return

    # create the directory
    pathlib.Path(dinucleotide_dist_data_dir).mkdir(parents=True, exist_ok=True)

    # download the required annotation files
    utils.download_gencode_files(version=version)

    regions = ["five_prime_UTR", "three_prime_UTR", "CDS"]
    main_gff_file = os.path.join(DATA_DIR, 'gencode_files', version, 'gencode.' + version + '.annotation.gff3')
    main_gff_df = utils.parse_annotation_file(main_gff_file)
    for region in regions:
        out_file = os.path.join(DATA_DIR, "dinucleotide_distributions", version, region + "_distribution.txt")
        output_region_distribution(main_gff_df, region, out_file)

    region = "noncoding"
    lncRNA_gff_file = os.path.join(DATA_DIR, 'gencode_files', version, 'gencode.' + version + '.long_noncoding_RNAs.gff3')
    lncRNA_gff_df = utils.parse_annotation_file(lncRNA_gff_file)
    out_file = os.path.join(DATA_DIR, "dinucleotide_distributions", version, region + "_distribution.txt")
    output_region_distribution(lncRNA_gff_df, region, out_file)


def output_region_distribution(annotation_df, region, out_file):
    """
    Given an mRNA region, get the dinucleotide distributions and output the file
    :param annotation_df: annotation df to get region coordinates from
    :param region: region to get distribution of
    :param out_file: name of output file
    :return: N/A
    """
    # make sure the chromosome files are downloaded
    utils.download_genome_chr_files(version="hg38")

    chromosomes = ["chr" + str(x) for x in range(1,23)] + ["chrX", "chrY", "chrM"]
    sequence_list = []
    for chromosome in tqdm(chromosomes):
        positive_coordinates, negative_coordinates = get_list_of_region_coordinates(annotation_df, region)
        positive_coordinates = merge_overlapping_coordinates(positive_coordinates)
        negative_coordinates = merge_overlapping_coordinates(negative_coordinates)
        sequence_list += get_sequences(positive_coordinates, chromosome, strand='+')
        sequence_list += get_sequences(negative_coordinates, chromosome, strand='-')
    dinucleotide_counts = defaultdict(int)

    # get dinucleotide distribution
    dinucleotide_counts = get_dinucleotide_counts(sequence_list, dinucleotide_counts)

    # write distribution to out_file
    with open(out_file, 'w') as f:
        for dinucleotide in dinucleotide_counts:
            f.write(dinucleotide + ":" + str(dinucleotide_counts[dinucleotide]) + "\n")


def get_list_of_region_coordinates(annotation_df, region="three_prime_UTR", chr="chr1"):
    """
    Get coordinates given a transcript region and chromosome
    :param annotation_df: df of the main annotation file
    :param region: the transcript region
    :param chr: the chromosome
    :return: a list of positive strand coordinates and a list of negative strand coordinates from the specified
    transcript region and chromosome
    """
    # if region is noncoding, don't filter by region
    if region == "noncoding":
        coordinates = annotation_df[annotation_df['chr'] == chr]
    else:
        coordinates = annotation_df[(annotation_df['region'] == region) & (annotation_df['chr']==chr)]
    # don't print warnings
    pd.options.mode.chained_assignment = None  # default='warn'
    # -1 because gff coordinates are 1 based and start and end inclusive
    # We want to convert to 0 based and end exclusive
    coordinates['start'] = coordinates.start.sub(1)
    positive_coordinates = coordinates[coordinates['strand'] == '+'][["start", "end"]].values.tolist()
    negative_coordinates = coordinates[coordinates['strand'] == '-'][["start", "end"]].values.tolist()
    return positive_coordinates, negative_coordinates


def merge_overlapping_coordinates(coordinates):
    """
    This function takes a list of coordinates from a gff file of a transcript region
    and merge overlapping coordinates to avoid double counting dinucleotides
    :param coordinates: a list of coordinates of a transcript region
    :return: a list of merged overlapping coordinates
    """
    # the coordinates are start inclusive and end exclusive e.g. [1,2)
    coordinates = [P.closedopen(coordinate[0], coordinate[1]) for coordinate in coordinates]
    merged_coordinates = P.Interval(*coordinates)
    ret = []
    for coordinate in list(merged_coordinates):
        start = int(P.to_data(coordinate)[0][1])
        end = int(P.to_data(coordinate)[0][2])
        assert start < end
        ret.append([start, end])
    return ret


def get_overlapping_region_distribution(seq_file, g4_seq_file):
    seq_df = pd.read_csv(seq_file, sep="\t")
    g4_seq_df = pd.read_csv(g4_seq_file, sep="\t")
    idx_list = []
    for idx, row in tqdm(g4_seq_df.iterrows()):
        df = seq_df[seq_df['sequence'].str.contains(row["sequence"])]
        if not df.empty:
            idx_list.append(idx)
    # data_exploration.get_count_per_category(g4_seq_df.iloc[idx_list], "gene_regions")


def get_sequence_dinucleotide_counts(sequence, dinucleotide_counts):
    for i in range(len(sequence) - 1):
        dinucleotide = sequence[i:i + 2]
        # don't include dinucleotide with N nucleotide in the count
        if 'N' not in dinucleotide:
            dinucleotide_counts[dinucleotide] += 1
    return dinucleotide_counts


def get_dinucleotide_counts(sequence_list, dinucleotide_counts):
    for sequence in tqdm(sequence_list):
        dinucleotide_counts = get_sequence_dinucleotide_counts(sequence, dinucleotide_counts)
    return dinucleotide_counts


def get_sequences(coordinates, chr, strand='+', genome_ver="hg38"):
    chr_file_name = chr + ".fa.gz"
    chr_file = os.path.join(DATA_DIR, genome_ver, chr_file_name)
    # get the current chromosome sequence
    with gzip.open(chr_file, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            sequence = record
            if strand == '-':
                sequence = record.reverse_complement()

    sequence_list = []
    for coordinate in coordinates:
        sequence_list.append(str(sequence[coordinate[0]:coordinate[1]].seq).upper())
    return sequence_list


if __name__ == '__main__':
    # output dinucleotide distribution files for each mRNA region: 5'UTR, 3'UTR, CDS, long noncoding
    output_all_region_distributions()


"""
This file contains functions used to analyze G4mer prediction results on wildtype 5'utr sequences
"""

import numpy as np
import pandas as pd
import os
from tqdm import tqdm
from sklearn.metrics import roc_curve
import re


DATA_DIR = os.path.join("/", "data", "g4mer", "utr")


def run_all():
    df = filter_utr_window_by_max_score()
    filter_utr_by_threshold(df)


def analyze_score_distribution_across_windows():
    # Map the predicted scores of the sequences to all utr sequence windows
    # Score file and sequence file should be a one to one mapping. Sequence file is the file used to generate the
    # g4mer 6mer input format to get the scores of
    df = append_scores(
        scores_file=os.path.join(DATA_DIR, "wildtype", "dnabert_output", "utr_wildtype", "pred_results.npy"),
        sequences_file=os.path.join(DATA_DIR, "wildtype", "unique_utr_sequences_70nt_cleaned.csv"),
        out_file=os.path.join(DATA_DIR, "wildtype", "all_utr_sequences_70nt_pred_scores.csv"),
        sequence_column='sequence',
        score_column='score'
    )

    # find sequences with exactly one canonical g4
    out_dir = os.path.join(DATA_DIR, "wildtype", "canonical_g4")
    filename = os.path.join(out_dir, "all_utr_sequences_70nt_pred_scores_one_canonical_g4.csv")
    if not os.path.exists(filename):
        df = find_sequences_with_canonical_g4(df)
        df.to_csv(
            filename,
            sep='\t',
            index=None
        )

    # get the g4 length distribution



def find_sequences_with_canonical_g4(df):
    df = df.reset_index(drop=True)
    # definition of canonical g4
    regex = "[G]{3}[AGTC]{1,7}[G]{3}[AGTC]{1,7}[G]{3}[AGTC]{1,7}[G]{3}"
    index_list = []
    for index, row in tqdm(df.iterrows()):
        sequence = row.original_sequence
        assert len(sequence) >= 6
        assert len(sequence) <= 70
        # if sequence contains canonical G4, check if mutation is on G4
        if re.search(regex, sequence):
            # coordinates of canonical g4 in the sequence
            coords = [(m.start(0), m.end(0)) for m in re.finditer(regex, sequence)]
            if len(coords) == 1:
                index_list.append(index)

    # return rows with sequences containing exactly one canonical G4
    return df.iloc[index_list]


def filter_utr_by_threshold(df):
    """
    Get only utr sequences with max score greater than or equal to the threshold
    :param df:
    :return:
    """
    threshold = find_optimal_cutoff()
    df = df[df['score'] >= threshold]
    filename = os.path.join(DATA_DIR, "wildtype", "filtered_max_score_utr_sequences_70nt.csv")
    df.to_csv(filename, sep='\t', index=None)
    print("Number of rows in filtered_max_score_utr_sequences_70nt.csv is", len(df))
    return df


def filter_utr_window_by_max_score():
    return get_max_score_window()


def get_max_score_window(df):
    """
    For each 5'utr sequence, get the max 70nt long window with the maximum prediction score
    :return:
    """
    print("Starting to get max prediction score per full length 5'utr sequence...")
    idx = df.groupby(['original_sequence'])['score'].transform(max) == df['score']
    df = df[idx]
    # drop duplicates, some utr sequences have multiple windows with the same max score
    df = df.drop_duplicates(subset='transcript_name', keep="first")
    df.to_csv("data/max_score_utr_sequences_70nt.csv", sep='\t', index=None)
    print("Number of rows in max_score_utr_sequences_70nt.csv is", len(df))
    print("Done!!")
    print(df.head())
    return df


def append_scores(scores_file, sequences_file, out_file,
                  sequence_column='sequence', score_column='score'):
    """
    Append prediction scores of each unique max 70nt long utr sequences to the sequences
    :return : dataframe of the sequence and the dnabert prediction score
    """

    if os.path.exists(out_file):
        print("Output file exists, skipping score appending process")
        return

    # load the scores and sequences used in prediction
    print("Starting to append prediction scores to utr sequences...")
    scores = np.load(scores_file)
    print("Loaded scores...")
    sequences = pd.read_csv(sequences_file, sep='\t')
    window_sequences = sequences[sequence_column]
    print("Loaded all", len(window_sequences), "70nt sequences...")

    assert len(scores) == len(window_sequences)

    # append scores to the sequences
    df = pd.DataFrame()
    df[sequence_column] = window_sequences
    df[score_column] = scores

    # map the sequences and scores to all utr sequences so that there are prediction scores for all qualifying
    # transcripts with 5'utrs of length >= 6nt
    all_utr_file = os.path.join(DATA_DIR, 'all_utr_sequences_70nt.csv')
    all_utr_df = pd.read_csv(all_utr_file, sep='\t')
    print("Loaded all", len(all_utr_df), "sequences...")
    df = pd.merge(all_utr_df, df, on=sequence_column)
    # assert that we have prediction scores for all sequences of length 6nt and above
    assert len(df) == len(all_utr_df[all_utr_df[sequence_column].str.len() >= 6])
    df.to_csv(out_file, index=None, sep='\t')
    print("Done!!")

    return df


def find_optimal_cutoff():
    """
    Calcualte the J youden's index of G4mer trained on rg4seeker data
    :return:
    """
    y_true = pd.read_csv(os.path.join(DATA_DIR, "train_dev", "rg4seeker", "binary", "dev.tsv"), sep='\t')['label']
    y_score = np.load(
        "/home/fzhuang/dnabert/DNABERT/examples/output/rg4seeker/binary_test/5_epochs/70n/pred_results.npy"
    )
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    idx = np.argmax(tpr - fpr)
    print("The optimal cutoff for G4mer is", thresholds[idx])
    return thresholds[idx]


if __name__ == '__main__':
    run_all()
# find the distribution of deltas with respect to relative position in canonical g4's

import pandas as pd
import re

file = "/data/g4mer/analysis/best_mutation_per_position_70nt.csv"
df = pd.read_csv(file, sep='\t')
print(df.columns)

# find original_sequence (max window per utr sequence) containing canonical g4 according to david lee's definition
def find_mutations_on_canonical_g4(df):
    df = df.reset_index(drop=True)
    regex = "[G]{3}[AGTC]{1,7}[G]{3}[AGTC]{1,7}[G]{3}[AGTC]{1,7}[G]{3}"
    index_list = []
    for index, row in df.iterrows():
        sequence = row.sequence
        assert len(sequence) <= 70
        # if sequence contains canonical G4, check if mutation is on G4
        if re.search(regex, sequence):
            # coordinates of canonical g4 in the sequence
            coords = [(m.start(0), m.end(0)) for m in re.finditer(regex, sequence)]
            # get the coordinate of the mutation
            mutation_coord = [i for i in range(len(row.mutated_sequence)) if row.mutated_sequence[i] != row.sequence[i]]
            # if len(mutation_coord) != 1:
            #     print(mutation_coord)
            #     print(sequence)
            #     print(row.mutated_sequence)
            # assert len(mutation_coord) == 1
            for coord in coords:
                if coord[0] <= mutation_coord[-1] <= coord[1]:
                    index_list.append(index)
    # return rows with sequences containing canonical G4
    return df.iloc[index_list]


def test_regex():
    regex = "[G]{3}[AGTC]{1,7}[G]{3}[AGTC]{1,7}[G]{3}[AGTC]{1,7}[G]{3}"
    # should not return anything because there are 8 A's after the first G run
    text = "GGGAGGGAGGGAGGG"
    ret = [(m.start(0), m.end(0)) for m in re.finditer(regex, text)]
    print(ret)


df = find_mutations_on_canonical_g4(df)
print(len(df))
#!/bin/bash

# script to get predicted scores of wildtype utr sequences

# activate dnabert virtual environment
. env_bert/bin/activate

export MAX_SEQ_LENGTH=70
export EPOCHS=5
export KMER=6
export DNABERT_PATH=/home/fzhuang/dnabert/DNABERT/examples
export MODEL_PATH=$DNABERT_PATH/output/rg4seeker/binary/${EPOCHS}_epochs/${MAX_SEQ_LENGTH}n/
export DATA_PATH=/data/g4mer/utr/wildtype/dnabert_input/70nt_6mer_GRCh38_v40/
export PREDICTION_PATH=$DNABERT_PATH/output/rg4seeker/binary_test/${EPOCHS}_epochs/${MAX_SEQ_LENGTH}n
export PRED_RESULT_OUTPUT_DIR=/home/fzhuang/g4/g4/dnabert_scripts/output/${EPOCHS}_epochs/${MAX_SEQ_LENGTH}n
export PRED_RESULT_OUTPUT_FILE=${PRED_RESULT_OUTPUT_DIR}/out

mkdir -p ${PREDICTION_PATH}
rm -rf ${PREDICTION_PATH}*

mkdir -p ${PRED_RESULT_OUTPUT_DIR}

python $DNABERT_PATH/run_finetune.py \
    --model_type dna \
    --tokenizer_name=dna$KMER \
    --model_name_or_path $MODEL_PATH \
    --task_name dnaprom \
    --do_predict \
    --pred_result_output_file $PRED_RESULT_OUTPUT_FILE \
    --data_dir $DATA_PATH  \
    --max_seq_length $MAX_SEQ_LENGTH \
    --per_gpu_pred_batch_size=128   \
    --output_dir $MODEL_PATH \
    --predict_dir $PREDICTION_PATH \
    --n_process 48

# deactivate virtual environment
conda deactivate
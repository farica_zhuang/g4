"""
This file contains shared functions across the project
"""

import os
import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import train_test_split
import urllib
import pathlib
from urllib import request
import gzip
from Bio import SeqIO
from Bio.Seq import Seq


DATA_DIR = os.path.join("/", "data", "g4mer")
chr_list = [x for x in range(1, 23)] + ['X', 'Y', 'M']


def download_genome_chr_files(version="hg38"):
    """
    Download chromosome files to get sequences per chromosome
    :param version: the genome version of the chromosomes to download
    :return: N/A
    """
    print("\nDownloading chromosome files\n")
    # create the directory
    destination_dir = os.path.join(DATA_DIR, version)
    pathlib.Path(destination_dir).mkdir(parents=True, exist_ok=True)

    for chr in tqdm(chr_list):
        chr = "chr" + str(chr)
        file_name = chr+".fa.gz"
        destination_file = os.path.join(destination_dir, file_name)
        # download file only if it doesn't exist
        if not os.path.isfile(destination_file):
            request.urlretrieve('ftp://hgdownload.cse.ucsc.edu/goldenPath/' + version + '//chromosomes/' + file_name,
                                destination_file)
    print("\nDownloaded chromosome files\n")


def download_gencode_files(version="v29"):
    """
    Download the required gencode files
    :param version: gencode version
    :return: N/A
    """
    print("\nDownloading gencode files...\n")
    class TqdmUpTo(tqdm):
        """Provides `update_to(n)` which uses `tqdm.update(delta_n)`."""
        def update_to(self, b=1, bsize=1, tsize=None):
            """
            b  : int, optional
                Number of blocks transferred so far [default: 1].
            bsize  : int, optional
                Size of each block (in tqdm units) [default: 1].
            tsize  : int, optional
                Total size (in tqdm units). If [default: None] remains unchanged.
            """
            if tsize is not None:
                self.total = tsize
            return self.update(b * bsize - self.n)  # also sets self.n = b * bsize

    destination_file = os.path.join(DATA_DIR, "gencode_files", version)
    pathlib.Path(destination_file).mkdir(parents=True, exist_ok=True)
    files = ["gencode." + version + ".annotation.gff3",
             "gencode." + version + ".long_noncoding_RNAs.gff3",
             "gencode." + version + ".transcripts.fa"]
    for file in files:
        # download file only if it doesn't exist
        if not os.path.isfile(os.path.join(destination_file, file)):
            print("\nDownloading", file, "...\n")
            file += ".gz"
            filename = os.path.join(destination_file, file)
            link = "https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_" + version[1:] + "/" + file
            request = getattr(urllib, 'request', urllib)
            with TqdmUpTo(unit='B', unit_scale=True, unit_divisor=1024, miniters=1,
                          desc=link.split('/')[-1]) as t:  # all optional kwargs
                request.urlretrieve(link, filename=filename,
                                    reporthook=t.update_to)
                t.total = t.n
            # unzip
            os.system('gzip -d ' + filename)

    # assert that the files are downloaded
    for file in files:
        assert os.path.exists(os.path.join(destination_file, file))

    print("\nDownloaded gencode files...\n")


def parse_annotation_file(annotation_file):
    """
    Parse an annotation file into dataframe of chromosome, region, start, end, strand, isoform
    :param annotation_file: path to the annotation file to be parsed
    :return: dataframe of transcript information from the given annotation file
    """
    print("\nParsing annotation file...\n")
    annotation_df = pd.read_csv(annotation_file, sep='\t', comment='#')
    df = pd.DataFrame()
    df['chr'] = annotation_df.iloc[:, 0]
    df['region'] = annotation_df.iloc[:, 2]
    df['start'] = annotation_df.iloc[:, 3]
    df['end'] = annotation_df.iloc[:, 4]
    df['strand'] = annotation_df.iloc[:, 6]
    isoform_list = []
    for idx, row in tqdm(annotation_df.iterrows()):
        names = row[8].split(';')
        isoform = ''
        for name in names:
            key = name.split('=')[0]
            if key == 'transcript_name':
                isoform = name.split('=')[1]
                break
        isoform_list.append(isoform)
    df['isoform'] = isoform_list
    print("\nParsed annotation file...\n")
    return df


def generate_train_dev_file(seq_df, dev_rate=0.1, train_out="train.tsv", dev_out="dev.tsv", seed=42):
    """
    Get sequence and labels dataframe and generate train file and dev files
    :param seq_df: dataframe containing "sequence" and "label"
    :param dev_rate: percentage of the sequences used for test file
    :param train_out: name of train file
    :param dev_out: name of test file
    :param seed: the random state for splitting the data
    :return: N/A
    """
    # get the sequence and label columns only
    seq_df = seq_df[['sequence', 'label']]
    seq_df_train, seq_df_test = train_test_split(seq_df, test_size=dev_rate, random_state=seed, stratify=seq_df.label)
    seq_df_train.to_csv(train_out, sep='\t', index=None)
    seq_df_test.to_csv(dev_out, sep='\t', index=None)


def get_chr_lengths(version="hg19"):
    chr_len = dict()
    destination_file = os.path.join(DATA_DIR, version, "chr_lengths.txt")
    if not os.path.isfile(destination_file):
        for chr in tqdm(chr_list):
            chr_file_name = "chr" + str(chr) + ".fa.gz"
            chr_file = os.path.join(DATA_DIR, version, chr_file_name)
            with gzip.open(chr_file, "rt") as handle:
                for record in SeqIO.parse(handle, "fasta"):
                    chr_len[chr] = len(record)
        with open(destination_file, 'w') as file:
            for key, value in chr_len.items():
                file.write("chr%s\t%s\n" % (key, value))


def get_sequences_from_coordinates(input_dict, version="hg19", orientation=False, find_g4_motif=False):
    """
    :param input_dict: input dict containing "start" and "end" coordinates
    :param version: genome version, hg19 or hg38
    :param orientation: boolean, false if orientation is not given, true if given
    :check_gruns: boolean, true if we want to ensure sequence contains gruns
    :return: input_dict containing "sequence"
    """
    for chr in tqdm(input_dict):
        # get the chromosome filename we're reading from
        if "chr" not in chr:
            prefix = "chr"
        else:
            prefix = ""
        chr_file_name = prefix + str(chr) + ".fa.gz"
        chr_file = os.path.join(DATA_DIR, version, chr_file_name)
        # get the current chromosome sequence
        with gzip.open(chr_file, "rt") as handle:
            for record in SeqIO.parse(handle, "fasta"):
                sequence = record
        seq_list = input_dict[chr]
        for i in range(len(seq_list)):
            start = seq_list[i]["start"]
            end = seq_list[i]["end"]
            curr_seq = str(sequence[start:end].seq).upper()
            if orientation:
                if seq_list[i]["orientation"] == -1:
                    curr_seq = get_reverse_complement(curr_seq)
            if find_g4_motif:
                curr_seq = find_g4_motif(curr_seq)
            input_dict[chr][i]["sequence"] = curr_seq
    return input_dict


def get_sequences_from_coordinates(input_dict, version="hg19", orientation=False, find_g4_motif=False):
    """
    :param input_dict: input dict containing "start" and "end" coordinates
    :param version: genome version, hg19 or hg38
    :param orientation: boolean, false if orientation is not given, true if given
    :check_gruns: boolean, true if we want to ensure sequence contains gruns
    :return: input_dict containing "sequence"
    """
    for chr in tqdm(input_dict):
        # get the chromosome filename we're reading from
        if "chr" not in chr:
            prefix = "chr"
        else:
            prefix = ""
        chr_file_name = prefix + str(chr) + ".fa.gz"
        chr_file = os.path.join(DATA_DIR, version, chr_file_name)
        # get the current chromosome sequence
        with gzip.open(chr_file, "rt") as handle:
            for record in SeqIO.parse(handle, "fasta"):
                sequence = record
        seq_list = input_dict[chr]
        for i in range(len(seq_list)):
            start = seq_list[i]["start"]
            end = seq_list[i]["end"]
            curr_seq = str(sequence[start:end].seq).upper()
            if orientation:
                if seq_list[i]["orientation"] == -1:
                    curr_seq = get_reverse_complement(curr_seq)
            if find_g4_motif:
                curr_seq = find_g4_motif(curr_seq)
            input_dict[chr][i]["sequence"] = curr_seq
    return input_dict


def get_reverse_complement(sequence):
    sequence = Seq(sequence)
    return str(sequence.reverse_complement().seq)


def convert_label(input_dict, neg_string="", pos_string=""):
    """
    Convert labels from string to 0 for negative and 1 for positive
    :return:
    """
    for chr in input_dict:
        coord_list = input_dict[chr]
        for i in range(len(coord_list)):
            if coord_list[i]["label"] == neg_string:
                coord_list[i]["label"] = 0
            elif coord_list[i]["label"] == pos_string:
                coord_list[i]["label"] = 1
            else:
                print("Label unknown")
    return input_dict


def get_kmer(seq_df, k=6):
    """
    Convert sequences into a string of kmer sequences for DNAbert input
    :param seq_df:
    :param k:
    :return:
    """
    seq_list = []
    res_df = seq_df.copy(deep=True)
    for index, row in seq_df.iterrows():
        sequence = row["sequence"]
        kmer_list = []
        for i in range(len(sequence) - k + 1):
            kmer_list.append(sequence[i:i+k])
        seq_list.append(" ".join(kmer_list))
    res_df["sequence"] = seq_list
    return res_df


if __name__ == '__main__':
    download_gencode_files()
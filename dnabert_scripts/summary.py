import os
import numpy as np
# get the metric for each fold

metric = "auc"
folder = "g4rna_cv"
folds = [str(x) for x in range(1,11)]
ret = []
for fold in folds:
    with open(os.path.join(folder, "fold_" + fold), "r") as f:
        for line in f:
            line = line.split("=")
            if line[0] == metric:
                ret.append(float(line[1]))
                break

print("Mean", metric, "is", np.mean(ret))
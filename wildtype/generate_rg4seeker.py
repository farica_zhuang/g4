"""
This file contains functions to generate clean sequences consisting only one g4 sequence each
"""
from utils import utils, data_quality_check
import random
import math
import pandas as pd
from tqdm import tqdm
import os
from Bio import SeqIO
import pathlib

import generate_dinucleotide_distributions
import generate_nonG4

# Global variables
DATA_DIR = os.path.join("/", "data", "g4mer")
GENCODE_VERSION = "v29"
GENOME_VERSION = "hg38"


# ----------------------------Functions to parse rg4seeker data------------------------------------------
def extract_g4_sequences_and_categories():
    file_path = os.path.join(DATA_DIR, "rg4seeker", "SupplementaryFile2.xlsx")
    print("Extracting g4 sequences and categories...")
    # read rG4-seeker
    df = pd.read_excel(file_path)
    # clean up column names
    df.columns = [col_name.replace("\n", "") for col_name in df.columns]
    # rG4 structural class
    category_col_name = 'rG4 structural class'
    sequence_col_name = 'Sequence diagram (RTS sites are indicated by asterisks)'
    gene_col_name = 'Overlapping Gencode gene names'
    gene_region_col_name = 'Overlapping Gencode gene regions'
    genomic_intervals_col_name = 'Genomic intervals of rG4'

    out_list = []
    for index, row in tqdm(df.iterrows()):
        # gene column e.g. PMF1-BGLAP|ENSG00000260238.6,PMF1|ENSG00000160783.19, want to extract gene ids only
        # check if gene name exists
        if isinstance(row[gene_col_name], str):
            genes = [gene.split('|')[1].strip() for gene in row[gene_col_name].split(',')]
            genes = ','.join(genes)
        else:
            genes = 'N/A'
        # check if gene region exists
        if not isinstance(row[gene_region_col_name], str):
            region = 'N/A'
        else:
            region = row[gene_region_col_name]
        sequence = row[sequence_col_name].split('-')[1]
        category = row[category_col_name]
        genomic_intervals = row[genomic_intervals_col_name]
        chromosome = genomic_intervals.split(':')[0]
        start = genomic_intervals.split(':')[1].split('-')[0]
        end = genomic_intervals.split(':')[1].split('-')[1]
        strand = genomic_intervals.split(':')[2]
        out_list.append([sequence.strip(), category.strip(), genes, region, get_isoform(sequence.strip()), chromosome, start, end, strand])

    # write g4 sequences to file
    out_df = pd.DataFrame(out_list, columns=['sequence', 'category', 'gene', 'gene_regions', 'isoform', 'chr', 'start', 'end', 'strand'])
    outfile = os.path.join(DATA_DIR, "rg4seeker", "g4_sequences.txt")
    out_df.to_csv(outfile, sep='\t', index=None)


def get_isoform(sequence):
    """
    Find the first isoform in the sequence that the g4 sequence is in
    :param sequence: g4 sequence
    :return: isoform that the g4 sequence is in
    """
    transcript_file = os.path.join(DATA_DIR, "gencode_files", GENCODE_VERSION, "gencode." + GENCODE_VERSION + ".transcripts.fa")
    isoform = "N/A"
    with open(transcript_file, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            transcript =str(record.seq).upper()
            if sequence in transcript:
                isoform = record.id.split('|')[4]
                break

    return isoform


# -----------------------------Functions to generate binary rg4seeker data--------------------------------------
def generate_binary_data(lengths, version="v29"):
    print("Running the entire pipeline to generate clean multiclass rg4seeker sequences")

    utils.download_gencode_files(version)

    # parse file containing g4 sequences
    g4_seq_df = parse_g4_seq_file()

    transcript_file = os.path.join(DATA_DIR, "gencode_files", version, "gencode." + version + ".transcripts.fa")

    for length in lengths:
        out_dir = os.path.join(DATA_DIR, "train_dev", "rg4seeker", "binary", str(length) + "n")
        # generate extended g4 sequences
        get_extended_g4_sequences(g4_seq_df=g4_seq_df,
                                  transcript_file=transcript_file,
                                  out_dir=out_dir,
                                  extended_seq_len=length,
                                  mode="binary")

        g4_data = pd.read_csv(os.path.join(out_dir, "g4_data.txt"), sep='\t')
        final_num_of_non_g4 = len(g4_data)

        # generate extended non g4 sequences
        generate_nonG4.run_all(genome_ver=GENOME_VERSION,
                               final_num_of_non_g4=final_num_of_non_g4,
                               extended_seq_len=length,
                               out_dir=out_dir)

        # combine g4_data.txt and non_g4_data.txt into g4_data.txt
        non_g4_data = pd.read_csv(os.path.join(out_dir, "non_g4_data.txt"), sep='\t')
        df = pd.concat([g4_data, non_g4_data], ignore_index=True)
        df.to_csv(os.path.join(out_dir, "g4_data.txt"), sep='\t', index=None)
        os.remove(os.path.join(out_dir, "non_g4_data.txt"))


# ----------------------------Functions to generate multiclass rg4seeker data----------------------------------
def generate_multiclass_data(lengths, version="v29"):
    """
    Function to run the entire pipeline to generate clean multiclass g4 sequences
    :param lengths: list of sequence lengths to generate clean sequences of
    :param annotation_file: version of the gff file to be used
    :return: N/A
    """
    print("Running the entire pipeline to generate clean multiclass rg4seeker sequences")

    utils.download_gencode_files(version)

    # parse file containing g4 sequences
    g4_seq_df = parse_g4_seq_file()

    # generate extended multiclass data
    transcript_file = os.path.join(DATA_DIR, "gencode_files", version, "gencode." + version + ".transcripts.fa")
    for length in lengths:
        out_dir = os.path.join(DATA_DIR, "train_dev", "rg4seeker", "multiclass", str(length) + "n")
        get_extended_g4_sequences(g4_seq_df=g4_seq_df,
                                  transcript_file=transcript_file,
                                  out_dir=out_dir,
                                  extended_seq_len=length,
                                  mode="multiclass")

    # parse gff annotation file to get dinucleotide distribution of the different regions
    annotation_file = os.path.join(DATA_DIR, 'gencode_files', version, 'gencode.' + version + '.annotation.gff3')
    annotation_df = utils.parse_annotation_file(annotation_file)

    # obtain a dictionary of dinucleotide distribution of the different regions
    dinucleotide_dist = get_dinucleotide_dist(version=version)

    # get clean extended sequences
    for length in lengths:
        get_clean_sequences(annotation_df, g4_seq_df, dinucleotide_dist, length)

    print("Done generating clean multiclass rg4seeker sequences!!")


def generate_random_sequence(dinucleotide_counts, seq_len=10):
    """
    Generate a random sequence following the given dinucleotide counts to preserve dinucleotide distribution
    :param dinucleotide_counts: dictionary of dinucleotide counts
    :param seq_len: length of random sequence to be generated
    :return: a random sequence
    """
    dinucleotides = int(math.ceil(seq_len/2))
    sequence = ''.join(random.choices(list(dinucleotide_counts.keys()), weights=dinucleotide_counts.values(), k=dinucleotides))
    sequence = sequence[:seq_len]
    return sequence


def get_clean_sequences(annotation_df, g4_seq_df, dinucleotide_dist, length):
    """
    Replaces sequences in the g4_data file of a particular length with clean sequences containing only one g4 sequence
    :param annotation_df: annotation dataframe to obtain g4 sequence region from
    :param g4_seq_df: dataframe containing reference g4 sequences from raw rg4-seeker data
    :param dinucleotide_dist: dictionary of dinucleotide distribution of genomic regions
    :param length: extended length of the g4 sequences
    :return: N/A
    """
    print("Writing clean multiclass sequences to g4_data.txt for sequences of length", length, "\n")
    # g4_data.txt has sequence, label, and g4 sequence columns
    filename = os.path.join(DATA_DIR, "train_dev", "rg4seeker", "multiclass", str(length) + "n", "g4_data.txt")
    sequences = pd.read_csv(filename, sep='\t')
    clean_sequence_list = []
    for index, row in tqdm(sequences.iterrows()):
        clean_sequence_list.append(get_clean_sequence(row.sequence, row.g4_sequence, annotation_df, g4_seq_df, dinucleotide_dist))
    sequences["sequence"] = clean_sequence_list
    sequences.to_csv(filename, sep='\t', index=None)


def get_clean_sequence(sequence, main_g4_seq, annotation_df, g4_seq_df, dinucleotide_dist):
    """
    Given a sequence and the main g4 sequence in the sequence, remove all other g4 sequences found
    :param sequence:
    :param main_g4_seq:
    :param g4_seqs:
    :return: a list of sequences with only one g4 sequence in each
    """
    g4_seqs = g4_seq_df['sequence'].tolist()
    # some g4 sequences overlap with each other, so avoid replacing g4 sequence that overlaps with the main g4
    main_g4_start = sequence.find(main_g4_seq)
    main_g4_end = main_g4_start + len(main_g4_seq)
    for g4_seq in g4_seqs:
        # otherwise, if sequence contains other g4 sequences other than the main sequence
        if main_g4_seq != g4_seq and g4_seq not in main_g4_seq and g4_seq in sequence:
            g4_seq_start = sequence.find(g4_seq)
            g4_seq_end = g4_seq_start + len(g4_seq)
            # remove g4 sequence that doesn't overlap with the main sequence
            if main_g4_end < g4_seq_end:
                start = max(main_g4_end, g4_seq_start)
                end = g4_seq_end
            if g4_seq_start < main_g4_start:
                start = g4_seq_start
                end = min(main_g4_start, g4_seq_end)
            # when finding region, use the original g4_seq
            region = find_region(annotation_df, g4_seq_df, g4_seq)
            dinucleotide_counts = dinucleotide_dist[region]
            # but only replace part of the g4 sequence not overlapping with main g4
            g4_seq = sequence[start:end]
            random_sequence = generate_random_sequence(dinucleotide_counts=dinucleotide_counts, seq_len=len(g4_seq))
            # make sure that the random sequence generated is not the same as the original g4 sequence to remove
            while random_sequence == g4_seq:
                random_sequence = generate_random_sequence(dinucleotide_counts=dinucleotide_counts, seq_len=len(g4_seq))
            sequence = sequence[:start] + random_sequence + sequence[end:]
    return sequence


def get_dinucleotide_dist(version="v29"):
    """
    Get dinucleotide distributions for all regions
    :return: a dictionary of dinucleotide distributions
    """

    # make sure the dinucleotide distribution files exist
    generate_dinucleotide_distributions.output_all_region_distributions(version=version)

    regions = ['five_prime_UTR', 'three_prime_UTR', 'CDS', 'noncoding']
    dinucleotide_dist = dict()
    for region in regions:
        file = os.path.join(DATA_DIR, "dinucleotide_distributions", GENCODE_VERSION, region + '_distribution.txt')
        dinucleotide_count = dict()
        with open(file) as f:
            for line in f:
                key, value = line.split(':')
                dinucleotide_count[key] = int(value)
        dinucleotide_dist[region] = dinucleotide_count
    return dinucleotide_dist


def find_region(annotation_df, g4_seq_df, g4_seq):
    """
    Finds the genomic region a given g4 sequence is in
    :param annotation_df: annotation file dataframe
    :param g4_seq_df: g4 sequences dataframe
    :param g4_seq: the g4 sequence to find the region of
    :return: genomic region that the g4 sequence is in based on the provided annotation file
    """
    row = g4_seq_df[g4_seq_df['sequence'] == g4_seq]
    chromosome = row['chr'].iloc[0]
    isoform = row['isoform'].iloc[0]
    strand = row['strand'].iloc[0]
    start = row['start'].iloc[0]
    end = row['end'].iloc[0]
    regions = ['five_prime_UTR', 'three_prime_UTR', 'CDS']
    region = annotation_df[(annotation_df['chr'] == chromosome) &
                           (annotation_df['isoform'] == isoform) &
                           (annotation_df['strand'] == strand) &
                           (annotation_df['start'] <= start) &
                           (annotation_df['end'] >= end) &
                           (annotation_df['region'].isin(regions))]
    if len(region) == 0:
        region = 'noncoding'
    else:
        region = region['region'].iloc[0]
    return region


#-------------------------------------------Shared helper functions------------------------------------------
def parse_g4_seq_file():
    """
    Parse file with rg4seeker g4 sequences
    :return: dataframe containing rg4seeker g4 sequences
    """
    print("Parsing g4_sequences.txt to obtain list of reference g4 sequences")
    file = os.path.join(DATA_DIR, 'rg4seeker', 'g4_sequences.txt')
    if not os.path.exists(file):
        extract_g4_sequences_and_categories()
    g4_seq_df = pd.read_csv(file, sep='\t')
    return g4_seq_df


def get_extended_g4_sequences(g4_seq_df, transcript_file, out_dir, extended_seq_len, mode="binary"):
    """
    This function get RG4Seeker g4 sequences and extends them by mapping the g4 sequences
    to the transcipt in the transcript file containing all human genes and their
    transcript sequences, and then extending the g4 sequences by including sequences
    to the left and right of them.
    :param rg4_seeker_data: dictionary containing g4 sequences from RG4Seeker data
    :param transcript_file: transcript file we're mapping the g4 sequences to
    :param out_file:
    :param extended_seq_len: final length of the g4 sequences after extension
    :return: N/A
    """
    # create the directory for the output file
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    out_file = os.path.join(out_dir, "g4_data.txt")

    category_dict = {
        'G>=40%': 0,
        'Unknown': 1,
        'bulges': 2,
        'canonical/G3L1-7': 3,
        'longloop': 4,
        'potential G-quadruplex & G>=40%': 5,
        'potential G-triplex & G>=40%': 6,
        'two-quartet': 7
    }

    print("------------Getting isoforms from transcript file--------------")
    # read the transcript file and get the dictionary of transcripts
    transcript_dict = dict()
    with open(transcript_file, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            transcript =str(record.seq).upper()
            isoform = record.id.split('|')[4]
            transcript_dict[isoform] = transcript

    less_length = 0

    extended_seqs = []

    print("------------Getting extended g4 sequences--------------")
    with open(out_file, 'w+') as w1:
        if mode == 'binary':
            w1.write('sequence\tlabel\n')
        elif mode == 'multiclass':
            w1.write('sequence\tlabel\tg4_sequence\n')
        target_len = extended_seq_len
        for index, row in tqdm(g4_seq_df.iterrows()):
            if pd.isnull(row['isoform']) or row["isoform"] == "N/A":
                continue
            # shouldn't be printed
            if row.isoform not in transcript_dict:
                print(row.isoform)
            category = category_dict[row.category]
            rg4 = row['sequence']
            length = len(rg4)
            extend_len = int((target_len - length)/2)
            if length + 2*extend_len < target_len:
                length += 1
            seq = transcript_dict[row.isoform]
            index = seq.find(rg4)
            # if the g4 sequence is found
            if index != -1:
                # if the g4 sequence is on the extreme left of the transcript
                if index < extend_len:
                    extended_seq = seq[:target_len]
                # if the g4 sequence is on the extreme right of the transcript
                elif len(seq) < index + extend_len + length:
                    extended_seq = seq[-target_len:]
                # else the g4 sequence can be centered
                else:
                    extended_seq = seq[index - extend_len : index + extend_len + length]
                if len(extended_seq) != target_len:
                    less_length += 1
                    print(len(extended_seq))

                if mode == 'binary':
                    # only write the sequence to file if sequence doesn't exist yet
                    if extended_seq not in extended_seqs:
                        extended_seqs.append(extended_seq)
                        w1.write(extended_seq + '\t' + str(1) + '\n')
                elif mode == 'multiclass':
                    if extended_seq not in extended_seqs:
                        extended_seqs.append(extended_seq)
                        w1.write(extended_seq + '\t' + str(category) + '\t' + rg4 + '\n')

    print("Found", len(set(g4_seq_df['sequence'])), "unique sequences in rg4seeker")
    print("Wrote", len(set(extended_seqs)), "unique sequences to file")
    print("------------g4 data txt has been generated--------------------")


def generate_all_dnabert_input_data(lengths, kmers, mode="binary", split_to_train_dev=False, out_file="train.tsv"):
    """
    Function to run the entire pipeline of obtaining rg4seeker sequences as dnabert kmer input of fixed lengths
    Mode can be either binary or multiclass
    :param lengths:
    :param kmers:
    :param mode:
    :return:
    """
    assert mode in ['binary', 'multiclass']

    if mode == "binary":
        generate_binary_data(lengths=lengths, version=GENCODE_VERSION)
    elif mode == "multiclass":
        generate_multiclass_data(lengths=lengths, version=GENCODE_VERSION)

    # reference rg4seeker g4 sequences to check against in data quality check
    g4_file = os.path.join(DATA_DIR, "rg4seeker", "g4_sequences.txt")
    for length in lengths:
        extended_g4s_file = os.path.join(DATA_DIR, "train_dev", "rg4seeker", mode, str(length) + "n", "g4_data.txt")
        # data_quality_check.check_generate_rg4seeker_output(file_to_check=extended_g4s_file,
        #                                                    g4_file=g4_file,
        #                                                    expected_len=length,
        #                                                    mode=mode)
        # generate dnabert kmers input format
        for kmer in kmers:
            # if split to train and dev files
            if split_to_train_dev:
                # split data into train and dev
                out_dir = os.path.join(DATA_DIR, "train_dev", "rg4seeker", mode, str(length) + "n")
                pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
                train_out = os.path.join(out_dir, "train.tsv")
                dev_out = os.path.join(out_dir, "dev.tsv")
                seq_df = pd.read_csv(extended_g4s_file, sep='\t')
                utils.generate_train_dev_file(seq_df, dev_rate=0.1, train_out=train_out, dev_out=dev_out, seed=42)

                # get kmer
                out_dir = os.path.join(DATA_DIR, "train_dev", "rg4seeker", mode, str(kmer)+"mer", str(length) + "n")
                pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
                seq_df = utils.get_kmer(seq_df=seq_df, k=kmer)
                train_out = os.path.join(out_dir, "train.tsv")
                dev_out = os.path.join(out_dir, "dev.tsv")
                utils.generate_train_dev_file(seq_df, dev_rate=0.1, train_out=train_out, dev_out=dev_out, seed=42)

            # if not splitting to train and dev files
            else:
                out_dir = os.path.join(DATA_DIR, "rg4seeker", mode, str(kmer)+"mer", str(length) + "n")
                pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
                seq_df = pd.read_csv(extended_g4s_file, sep='\t')
                seq_df = utils.get_kmer(seq_df=seq_df, k=kmer)
                seq_df.to_csv(os.path.join(out_dir, out_file), sep='\t', index=None)


if __name__ == '__main__':
    lengths = [70]
    kmers = [6]

    # -----------------------generate multiclass data, extended to the specified lengths--------------------------------
    # generate_all_dnabert_input_data(lengths=lengths, kmers=kmers, mode="multiclass", split_to_train_dev=True)

    # -----------------------generate binary data, extended to the specified lengths------------------------------------
    # generate_all_dnabert_input_data(lengths=lengths, kmers=kmers, mode="binary", split_to_train_dev=True)

    # -----------------------generate binary data, not split to train dev to train g4mer--------------------------------
    generate_all_dnabert_input_data(lengths=lengths, kmers=kmers, mode="binary", split_to_train_dev=False, out_file="train.tsv")

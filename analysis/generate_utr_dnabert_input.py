"""
This file contains functions to generate cleaned, unique 5'utr sequences in the 6mer format accepted by G4mer with
length 6-70nt
"""

import pandas as pd
import os
from tqdm import tqdm
import pathlib


DATA_DIR = os.path.join("/", "data", "g4mer", "utr")

SEQUENCE_IDENTIFIER = "GRCh38_v40"

def create_directories():
    """
    This function creates the required directories to store wildtype and mutated utr sequences data
    :return: N/A
    """
    wildtype_dir = os.path.join(DATA_DIR, "wildtype")
    pathlib.Path(wildtype_dir).mkdir(parents=True, exist_ok=True)
    mutated_dir = os.path.join(DATA_DIR, "mutated")
    pathlib.Path(mutated_dir).mkdir(parents=True, exist_ok=True)

    print("All required data directory exist!!")


def run_all_wildtype():
    """
    This function calls all functions to generate wildtype 5utr sequences of length 6-70nt to get prediction scores of
    :return: N/A
    """
    # create the required wildtype data directory
    create_directories()

    print("Running workflow to generate wildtype utr sequences...")

    # break long utr sequences down into max length ot 70nt
    filename = os.path.join(DATA_DIR, "wildtype", "all_utr_sequences_70nt_" + SEQUENCE_IDENTIFIER + ".csv")
    df = extract_fixed_length_sequences(filename)

    # drop duplicated utr sequences of max length 70nt
    filename = os.path.join(DATA_DIR, "wildtype", "unique_utr_sequences_70nt_" + SEQUENCE_IDENTIFIER + ".csv")
    df = extract_unique_fixed_length_sequences(df, filename=filename, sequence_column="sequence")

    # remove utr sequences shorter than 6nt
    filename = os.path.join(DATA_DIR, "wildtype", "unique_utr_sequences_70nt_" + SEQUENCE_IDENTIFIER + "_cleaned.csv")
    df = extract_cleaned_data(df, filename=filename)

    # get 6mer data of the 6-70nt long utr sequences
    out_dir = os.path.join(DATA_DIR, "wildtype", "dnabert_input", "70nt_6mer_" + SEQUENCE_IDENTIFIER)
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(out_dir, "dev.tsv")
    extract_kmer_data(df, filename=filename)

    print("Done generating wildtype 5'utr sequences as G4mer input")


def run_all_mutate():
    """
    This function calls all functions to generate mutated 5utr sequences of length 6-70nt to get prediction scores of
    :return: N/A
    """
    # create the required wildtype data directory
    create_directories()

    print("Running workflow to generate mutated utr sequences...")

    # read input dataframe containing sequences of high probabilities of containing G4 sequence
    filename = os.path.join(DATA_DIR, "wildtype", "filtered_max_score_utr_sequences_70nt.csv")
    df = pd.read_csv(filename, sep='\t')
    print("Done loading data...found", len(df), "sequences to mutate")

    # mutate the sequences
    filename = os.path.join(DATA_DIR, "mutated", "mutated_filtered_max_score_utr_sequences_70nt_cleaned.csv")
    df = extract_mutated_sequences(df, filename=filename)

    # drop duplicates of mutated sequences
    filename = os.path.join(DATA_DIR, "mutated", "unique_mutated_filtered_max_score_utr_sequences_70nt_cleaned.csv")
    df = extract_unique_fixed_length_sequences(df, filename, sequence_column="mutated_sequence")

    # generate kmer data of unique mutated sequences
    out_dir = os.path.join(DATA_DIR, "mutated", "dnabert_input", "70nt_6mer")
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(out_dir, "dev.csv")
    extract_kmer_data(df, filename=filename, sequence_column='mutated_sequence')


def extract_fixed_length_sequences(filename):
    if not os.path.isfile(filename):
        print("Generating maximum 70nt sequences for all transcripts...")
        df = generate_fixed_length_sequences()
        df.to_csv(filename, index=None, sep='\t')
        print("Generated maximum 70nt sequences for all transcripts")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted maximum 70nt sequences for all transcripts...total sequences found:", len(df))
    return df


def extract_unique_fixed_length_sequences(df, filename, sequence_column="sequence"):
    """
    Break sequences longer than 70nt down into sequences of 70nt
    :param df: dataframe containing sequences longer than 70nt
    :param sequence_column: name of column containing sequences to remove duplicates of
    :return: dataframe containing sequences of maximum length of 70nt
    """
    if not os.path.isfile(filename):
        df = merge_duplicates(df, sequence_column=sequence_column)
        df.to_csv(filename, index=None, sep='\t')
        print("Generated unique maximum 70nt sequences for all transcripts")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted unique maximum 70nt sequences...total sequences found:", len(df))
    return df


def extract_kmer_data(df, filename, sequence_column='sequence'):
    """
    Convert sequences into kmers, generate a file of kmers
    :param df: dataframe of sequences
    :param filename: output file of kmers
    :param sequence_column: the name of the column containing sequences
    :return: N/A
    """
    if not os.path.isfile(filename):
        df = generate_kmer(df, sequence_column=sequence_column)
        df.to_csv(filename, index=None, sep='\t')
        print("Generated 6mers of unique maximum 70nt sequences for all transcripts")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted 6mers of unique maximum 70nt sequences...total sequences found:", len(df))
    return df


def extract_cleaned_data(df, filename):
    """
    Create a file of unique utr sequences with sequences shorter than 6nt removed
    :param df: dataframe of utrs of all lengths and a maximum of 70nt
    :return: dataframe of utrs between 6-70nt
    """
    if not os.path.isfile(filename):
        df = generate_cleaned_utr_sequences(df)
        df.to_csv(filename, index=None, sep='\t')
        print("Generated cleaned unique maximum 70nt sequences for all transcripts")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted cleaned unique maximum 70nt sequences...total sequences found:", len(df))
    return df


def extract_mutated_sequences(df, filename):
    if not os.path.isfile(filename):
        df = generate_mutated_sequences(df)
        df.to_csv(filename, index=None, sep='\t')
        print("Generated mutated unique maximum 70nt sequences for all transcripts")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted mutated unique maximum 70nt sequences...total sequences found:", len(df))
    return df


def extract_small_mutated_sequences(df):
    """
    Function to generate a small test data
    :param df:
    :return:
    """
    filename = "data/small_mutated_unique_utr_sequences_70nt_cleaned.csv"
    if not os.path.isfile(filename):
        df = generate_small_mutated_sequences(df)
        df.to_csv(filename, index=None, sep='\t')
        print("Generated small mutated cleaned unique maximum 70nt sequences for all transcripts")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted small mutated cleaned unique maximum 70nt sequences...total sequences found:", len(df))
    return df


def generate_fixed_length_sequences():
    """
    Obtain sequences with maximum length of 70 from the given list of utr sequences
    :return: N/A
    """
    filename = os.path.join(DATA_DIR, "wildtype", "all_utr_sequences_" + SEQUENCE_IDENTIFIER + ".csv")
    df = pd.read_csv(filename, sep='\t')
    df_row_list = []
    print("Processing", len(df), "sequences")
    for index, row in tqdm(df.iterrows()):
        original_sequence = row.sequence
        # if the sequence is more than 70nt long, break up the sequence into sequences of length 70nt
        if len(row.sequence) > 70:
            sequences = []
            for i in range(len(row.sequence)-70+1):
                sequences.append(row.sequence[i:i+70])
        else:
            sequences = [row.sequence]
        for sequence in sequences:
            df_row_list.append([
                row.chromosome, row.gene_id, row.transcript_id, row.gene_name,
                row.transcript_name, sequence, original_sequence
            ])

    df = pd.DataFrame(df_row_list, columns=['chromosome', 'gene_id', 'transcript_id', 'gene_name', 'transcript_name',
                                            'sequence', 'original_sequence'])

    # make sure that all sequences in the df is of max length 70nt
    assert len(df) == len(df[df['sequence'].str.len() <= 70])

    return df


def merge_duplicates(df, sequence_column):
    """
    This function merges fixed length sequences that appear more than once in the dataframe
    :param df: dataframe containing fixed length sequences
    :return: dataframe containing distinct fixed length sequences
    """
    sequences = df[sequence_column].unique()
    df = pd.DataFrame(sequences.tolist(), columns=[sequence_column])

    # assert that all sequences are unique
    assert len(df) == len(df[sequence_column].unique().tolist())

    return df


def generate_kmer(df, sequence_column='sequence', k=6):
    """
    This function takes a dataframe of sequences of max length 70nt and generates a file with 6mer sequences for G4mer
    :param df: dataframe with sequences of max length 70nt
    :param k: kmer to break the sequences into
    :return: dataframe with sequences converted to 6mer sequences
    """
    seq_list = []
    res_df = df.copy(deep=True)
    for index, row in tqdm(df.iterrows()):
        sequence = row[sequence_column]
        kmer_list = []
        for i in range(len(sequence) - k + 1):
            kmer_seq = sequence[i:i+k]

            # assert that we're extracting kmers from the full length sequence
            assert len(kmer_seq) == k

            kmer_list.append(kmer_seq)
        seq_list.append(" ".join(kmer_list))
    res_df["sequence"] = seq_list
    # need to have more than one class of label to avoid error when G4mer auto calculates auc for the test data
    res_df["label"] = [0 for _ in range(len(df)//2)] + [1 for _ in range(len(df)//2, len(df))]
    res_df = res_df[['sequence', 'label']]
    return res_df


def generate_cleaned_utr_sequences(df):
    """
    Remove sequences of length shorter than 6nt
    :param df: dataframe with sequences to be filtered
    :return: dataframe with filtered sequences
    """
    df = df[df['sequence'].str.len() >= 6]

    # assert that all sequence in the column are 6nt or longer
    assert len(df) == len(df[df['sequence'].str.len() >= 6])

    return df


def generate_mutated_sequences(df):
    """
    For each sequence in the given dataframe, mutate each nucleotide to other possible nucleotides
    :param df: dataframe with sequences to mutate
    :return: dataframe with mutated sequences
    """
    # assert len(df['sequence'].unique().tolist()) == len(df['original_sequence'].unique().tolist())

    all_nucleotides = ["A", "T", "G", "C"]
    df_row_list = []
    # for each sequence, get all possible mutations
    for index, row in tqdm(df.iterrows()):
        # get the sequence of length 70nt to mutate
        sequence = list(row.sequence)
        # for each nucleotide, get all possible mutations
        for i in range(len(sequence)):
            curr_nucleotide = sequence[i]
            for nucleotide in all_nucleotides:
                sequence = list(row.sequence)
                # mutate current nucleotide with other nucleotides
                if curr_nucleotide != nucleotide:
                    sequence[i] = nucleotide
                    df_row_list.append([
                        row.chromosome, row.gene_id, row.transcript_id, row.gene_name,
                        row.transcript_name, row.sequence, row.original_sequence, row.score,
                        ''.join(sequence)
                    ])

    df = pd.DataFrame(df_row_list, columns=['chromosome', 'gene_id', 'transcript_id', 'gene_name', 'transcript_name',
                                            'sequence', 'original_sequence', 'score', 'mutated_sequence'])

    # check that all mutated sequences are one nucleotide apart from the sequence
    for index, row in tqdm(df.iterrows()):
        assert diff_letters(row.sequence, row.mutated_sequence) == 1

    return df


def diff_letters(a, b):
    return sum(a[i] != b[i] for i in range(len(a)))


def generate_small_mutated_sequences(df):
    """
    For each sequence in the given dataframe, mutate each nucleotide to other possible nucleotides
    :param df: dataframe with sequences to mutate
    :return: dataframe with mutated sequences
    """
    # count = 1
    df_row_list = []
    for index, row in tqdm(df.iterrows()):
        # get the sequence of length 70nt to mutate
        sequence = row.sequence
        for i in range(len(sequence)):
            curr_nucleotide = sequence[i]
            # only mutate nucleotides that are not C
            if curr_nucleotide != 'C':
                temp_sequence = list(sequence)
                # mutate current nucleotide with C
                temp_sequence[i] = 'C'
                df_row_list.append([
                    row.chromosome, row.gene_id, row.transcript_id, row.gene_name,
                    row.transcript_name, row.sequence, row.original_sequence, row.score,
                    ''.join(temp_sequence)
                ])
        # if count == 100:
        #     break
        # count += 1

    df = pd.DataFrame(df_row_list, columns=['chromosome', 'gene_id', 'transcript_id', 'gene_name', 'transcript_name',
                                            'sequence', 'original_sequence', 'score', 'mutated_sequence'])
    return df


if __name__ == '__main__':
    run_all_wildtype()
    # run_all_mutate()
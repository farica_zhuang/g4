# Directory overview
This directory contains separate python files used to analyze utr sequences to find G4's and deleterious variants in.
The steps of the experiments are described below

### Generate utr sequences of all transcripts from gff annotation file
`generate_utr_sequences.py` contains functions used to generate utr sequences of all transcripts from gff annotation
file that provides coordinates of all 5' utr regions. The regions are combined per transcript and then the coordinates
used to obtain sequences

### Generate g4mer input of wildtype utr sequences
`generate_utr_dnabert_input.py` contains functions used to take the wildtype utr sequences that are cleaned and cut into
sequences of specified max length that G4mer can take in. The sequences are transformed into kmers so that it can
be passed into G4mer using `dnabert_scripts/test_wildtype_utr_sequences.sh`

### Analyze wildtype utr G4mer predictions
`analyze_wildtype_utr_predictions.py` contains functions used to analyze the wildtype utr sequences. The G4mer output
from the previous step contains prediction scores for G4s in each sequence. A couple things can be done here. First,
obtain all the 
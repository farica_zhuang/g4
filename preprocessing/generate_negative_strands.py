# This file takes non g4 sequences, extends to length 500, and ensures that it doesn't overlap with g4 sequences

# Coordinates are zero indexed, left inclusive and right exclusive

import pandas as pd
import math


def get_seq_dict(df, label, sort=True, sort_by="", extend=False, extended_length=0):
    seq_dict = dict()
    for index, row in df.iterrows():
        row_dict = dict()
        chromosome = row["chr"]
        start = row["start"]
        end = row["end"]
        orientation = row["orientation"]
        if chromosome not in seq_dict:
            seq_dict[chromosome] = []
        # extend the sequence by placing the original sequence in the middle
        if extend:
            seq_len = end - start
            to_add = (extended_length - seq_len) / 2
            if seq_len % 2 == 1:
                start_to_add = int(math.floor(to_add))
                end_to_add = int(math.ceil(to_add))
            else:
                start_to_add = int(to_add)
                end_to_add = int(to_add)
            row_dict["start"] = start - start_to_add
            row_dict["end"] = end + end_to_add
            row_dict["original_start"] = start
            row_dict["original_end"] = end
        else:
            row_dict["start"] = start
            row_dict["end"] = end
        row_dict["label"] = label
        row_dict["orientation"] = orientation
        seq_dict[chromosome].append(row_dict)

    if sort:
        for chromosome in seq_dict:
            seq_dict[chromosome].sort(key=lambda x: x[sort_by])

    return seq_dict


def find_non_overlapping_non_g4(g4_seq, non_g4_seq, max_chr_len, non_g4_seq_len=512):
    "Returns a dictionary of chromosomes as keys and list seq coordinates dictionaries as values"
    extended_non_g4_seq = {}
    for chromosome in non_g4_seq:
        if chromosome not in extended_non_g4_seq:
            extended_non_g4_seq[chromosome] = []
        combined_seq = g4_seq[chromosome] + non_g4_seq[chromosome]
        combined_seq.sort(key=lambda x: x["start"])
        # loop through the sequences to find non G4 seqs
        non_g4_idx = 0
        max_len = len(combined_seq)
        valid = True
        while non_g4_idx < max_len and valid:
            while combined_seq[non_g4_idx]["label"] != "nonG4":
                non_g4_idx += 1
                if non_g4_idx == max_len:
                    valid = False
                    break
            # if there is no more non g4 sequences in the list, exit the loop
            if not valid:
                break
            # otherwise, extend the sequence
            g4_idx1 = non_g4_idx
            while combined_seq[g4_idx1]["label"] != "G4":
                g4_idx1 -= 1
                if g4_idx1 == -1:
                    break
            g4_idx2 = non_g4_idx
            while combined_seq[g4_idx2]["label"] != "G4":
                g4_idx2 += 1
                if g4_idx2 == max_len:
                    break
            if g4_idx1 == -1:
                g4_coord1 = 0
            else:
                g4_coord1 = combined_seq[g4_idx1]["end"]
            if g4_idx2 == max_len:
                g4_coord2 = max_chr_len[chromosome]
            else:
                g4_coord2 = combined_seq[g4_idx2]["start"]
            # check if non g4 overlaps with g4s
            non_g4_start = int(combined_seq[non_g4_idx]["start"])
            non_g4_end = int(combined_seq[non_g4_idx]["end"])
            g4_left = int(g4_coord1)
            g4_right = int(g4_coord2)
            if non_g4_start < g4_left or non_g4_end > g4_right:
                # if the distance between the two G4s is too small, can't do anything
                if g4_right - g4_left < non_g4_seq_len:
                    break
                # otherwise, shift the non g4 such that it's not overlapping
                # with the original g4 coordinates included
                else:
                    if non_g4_start < g4_right:
                        to_shift = g4_left - non_g4_start
                    elif non_g4_end > g4_left:
                        to_shift = g4_right - non_g4_end
                    new_start = combined_seq[non_g4_idx]["start"] + to_shift
                    new_end = combined_seq[non_g4_idx]["end"] + to_shift
                    original_start = combined_seq[non_g4_idx]["original_start"]
                    original_end = combined_seq[non_g4_idx]["original_end"]
                    if new_start <= original_start < original_end <= new_end:
                        combined_seq[non_g4_idx]["start"] = new_start
                        combined_seq[non_g4_idx]["end"] = new_end
                        extended_non_g4_seq[chromosome].append(combined_seq[non_g4_idx])
            elif g4_left <= non_g4_start < non_g4_end <= g4_right:
                extended_non_g4_seq[chromosome].append(combined_seq[non_g4_idx])
            non_g4_idx += 1

    return extended_non_g4_seq

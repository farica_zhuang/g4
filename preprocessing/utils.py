"""
This file contains functions to prepare data for preprocessing
"""
from urllib import request
import os
from tqdm import tqdm
import gzip
from Bio import SeqIO
from Bio.Seq import Seq
import pandas as pd
import re
import math
import random

chr_list = [x for x in range(1, 23)] + ['X', 'Y']
data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
genome_ver = "hg38"

def download_genome_chr_files(version="hg19"):
    for chr in tqdm(chr_list):
        chr = "chr" + str(chr)
        file_name = chr+".fa.gz"
        destination_file = os.path.join(data_dir, version, file_name)
        # download file only if it doesn't exist
        if not os.path.isfile(destination_file):
            request.urlretrieve('ftp://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/'+file_name,
                            destination_file)

def get_chr_lengths(version="hg19"):
    chr_len = dict()
    destination_file = os.path.join(data_dir, version, "chr_lengths.txt")
    if not os.path.isfile(destination_file):
        for chr in tqdm(chr_list):
            chr_file_name = "chr" + str(chr) + ".fa.gz"
            chr_file = os.path.join(data_dir, version, chr_file_name)
            with gzip.open(chr_file, "rt") as handle:
                for record in SeqIO.parse(handle, "fasta"):
                    chr_len[chr] = len(record)
        with open(destination_file, 'w') as file:
            for key, value in chr_len.items():
                file.write("chr%s\t%s\n" % (key, value))

def get_sequences_from_coordinates(input_dict, version="hg19", orientation=False, find_g4_motif=False):
    """
    :param input_dict: input dict containing "start" and "end" coordinates
    :param version: genome version, hg19 or hg38
    :param orientation: boolean, false if orientation is not given, true if given
    :check_gruns: boolean, true if we want to ensure sequence contains gruns
    :return: input_dict containing "sequence"
    """
    for chr in tqdm(input_dict):
        # get the chromosome filename we're reading from
        if "chr" not in chr:
            prefix = "chr"
        else:
            prefix = ""
        chr_file_name = prefix + str(chr) + ".fa.gz"
        chr_file = os.path.join(data_dir, version, chr_file_name)
        # get the current chromosome sequence
        with gzip.open(chr_file, "rt") as handle:
            for record in SeqIO.parse(handle, "fasta"):
                sequence = record
        seq_list = input_dict[chr]
        for i in range(len(seq_list)):
            start = seq_list[i]["start"]
            end = seq_list[i]["end"]
            curr_seq = str(sequence[start:end].seq).upper()
            if orientation:
                if seq_list[i]["orientation"] == -1:
                    curr_seq = get_reverse_complement(curr_seq)
            if find_g4_motif:
                curr_seq = find_g4_motif(curr_seq)
            input_dict[chr][i]["sequence"] = curr_seq
    return input_dict

def find_g4_motif(sequence):
    if check_g4_motif(sequence):
        return sequence
    reverse_complement = get_reverse_complement(sequence)
    if check_g4_motif(reverse_complement):
        return reverse_complement
    return ""

def get_reverse_complement(sequence):
    sequence = Seq(sequence)
    return str(sequence.reverse_complement().seq)

def check_g4_motif(seq):
    """
    Function cheks if sequence contains G4 motif GGG-{N-1:7}-GGG
    :param seq: Sequence to be checked
    :return: True if G4 motif exists in sequence
    """
    g4_motif = "GGG[A,T,G,C]{1,7}GGG"
    return re.search(g4_motif, seq)

def convert_label(input_dict, neg_string="", pos_string=""):
    """
    Convert labels from string to 0 for negative and 1 for positive
    :return: 
    """
    for chr in input_dict:
        coord_list = input_dict[chr]
        for i in range(len(coord_list)):
            if coord_list[i]["label"] == neg_string:
                coord_list[i]["label"] = 0
            elif coord_list[i]["label"] == pos_string:
                coord_list[i]["label"] = 1
            else:
                print("Label unknown")
    return input_dict

def convert_genome_version():
    """
    Generate g4 and non g4 bed files with chromosome, start, and end coordinates
    :return:
    """
    # read g4 and non-g4 files from David that have coordinates of hg19 version
    three_prime_g4_file = os.path.join(data_dir, "utrs", "utr3_g4_locs.zeroindexed.bed")
    three_prime_nong4_file = os.path.join(data_dir, "utrs", "utr3_nong4_locs.zeroindexed.bed")
    five_prime_g4_file = os.path.join(data_dir, "utrs", "utr5_g4_locs.zeroindexed.bed")
    five_prime_nong4_file = os.path.join(data_dir, "utrs", "utr5_nong4_locs.zeroindexed.bed")

    # get g4 sequences
    three_prime_g4_df = pd.read_csv(three_prime_g4_file, header=None, sep="\t").iloc[:, :3]
    five_prime_g4_df = pd.read_csv(five_prime_g4_file, header=None, sep="\t").iloc[:, :3]
    g4_df = pd.concat([three_prime_g4_df, five_prime_g4_df]).drop_duplicates()

    # get non-g4 sequences
    three_prime_non_g4_df = pd.read_csv(three_prime_nong4_file, header=None, sep="\t").iloc[:, :4]
    five_prime_non_g4_df = pd.read_csv(five_prime_nong4_file, header=None, sep="\t").iloc[:, :4]
    non_g4_df = pd.concat([three_prime_non_g4_df, five_prime_non_g4_df]).drop_duplicates()
    non_g4_to_del = pd.read_csv(os.path.join(data_dir, "utrs", "non_g4_coords_to_del.bed"),
                                header=None, comment='#', sep='\t')
    # remove chr
    non_g4_to_del[non_g4_to_del.columns[0]] = non_g4_to_del[non_g4_to_del.columns[0]].str.replace(r'chr', '')
    non_g4_df = pd.merge(non_g4_df, non_g4_to_del, indicator=True, how='outer').query('_merge=="left_only"').drop('_merge', axis=1)

    # Write the dataframe to file
    g4_df[g4_df.columns[0]] = "chr" + g4_df[g4_df.columns[0]]
    non_g4_df[non_g4_df.columns[0]] = "chr" + non_g4_df[non_g4_df.columns[0]]
    g4_coords_to_convert_file = os.path.join(data_dir, "utrs", "g4_coords.hg19.bed")
    non_g4_coords_to_convert_file = os.path.join(data_dir, "utrs", "non_g4_coords.hg19.bed")
    non_g4_coords_to_convert_with_orientation_file = os.path.join(data_dir, "utrs", "non_g4_coords_with_orientation.hg19.bed")

    g4_df.to_csv(g4_coords_to_convert_file, sep=' ', header=False, index=False)
    non_g4_df.iloc[:, :3].to_csv(non_g4_coords_to_convert_file, sep=' ', header=False, index=False)
    non_g4_df.to_csv(non_g4_coords_to_convert_with_orientation_file, sep=' ', header=False, index=False)

def generate_test_train_file(sequence_list, test_rate=0.1, dev_rate=0.1, test_out="test.tsv", train_out="train.tsv", dev_out="dev.tsv"):
    """
    Get sequence and labels and generate train file and test file
    :param input_dict: list of dictionaries containing "sequence" and "label"
    :param test_rate: percentage of the sequences used for test file
    :param test_out: name of test file
    :param train_out: name of train file
    :return: N/A
    """
    random.shuffle(sequence_list)
    num_test = int(math.floor(len(sequence_list)*test_rate))
    num_dev = int(math.floor(len(sequence_list)*dev_rate))
    test = sequence_list[:num_test]
    with open(test_out, 'w') as file:
        file.write("sequence\tlabel\n")
        for sequence in test:
            if not 'N' in sequence["sequence"]:
                file.write("%s\t%s\n" % (sequence["sequence"], sequence["label"]))
    dev = sequence_list[num_test:num_test+num_dev]
    with open(dev_out, 'w') as file:
        file.write("sequence\tlabel\n")
        for sequence in dev:
            if not 'N' in sequence["sequence"]:
                file.write("%s\t%s\n" % (sequence["sequence"], sequence["label"]))
    # get dev data
    train = sequence_list[num_test+num_dev:]
    with open(train_out, 'w') as file:
        file.write("sequence\tlabel\n")
        for sequence in train:
            if not 'N' in sequence["sequence"]:
                file.write("%s\t%s\n" % (sequence["sequence"], sequence["label"]))


def check_non_overlapping_non_g4(non_overlapping_non_g4, g4_seq, extended_seq_len=0):
    """
    Sanity check
    Check that original start and end are within the extended sequence,
    or if original sequence is longer, then extended sequence is within the original sequence
    Check that there are no g4 coordinates in the extended sequence
    :param non_overlapping_non_g4:
    :return:
    """
    # check that sequence coordinates are valid
    for chr in non_overlapping_non_g4:
        sequence_list = non_overlapping_non_g4[chr]
        for sequence in sequence_list:
            original_seq_len = sequence["original_end"] - sequence["original_start"]
            # If original sequence length is shorter than extended sequence length
            # then the sequence was extended
            if original_seq_len < extended_seq_len:
                assert sequence["start"] <= sequence["original_start"] <= sequence["original_end"] <= sequence["end"]
            # otherwise, the sequence was cut
            else:
                assert sequence["original_start"] <= sequence["start"] <= sequence["end"] <= sequence["original_end"]

    # check that non g4 coordinates do not contain g4 coordinates
    non_g4_intervals = dict()
    g4_intervals = dict()
    for chr in non_overlapping_non_g4:
        if chr not in non_g4_intervals:
            non_g4_intervals[chr] = []
        if chr not in g4_intervals:
            g4_intervals[chr] = []

        non_g4_sequence_list = non_overlapping_non_g4[chr]
        for sequence in non_g4_sequence_list:
            non_g4_intervals[chr].append({
                "interval": pd.Interval(sequence["start"], sequence["end"], closed="left"),
                "orientation": sequence["orientation"]
            })
        g4_sequence_list = g4_seq[chr]
        for sequence in g4_sequence_list:
            g4_intervals[chr].append(
                {
                    "interval": pd.Interval(sequence["start"], sequence["end"], closed="left"),
                    "orientation": sequence["orientation"]
                 })
    final_non_overlapping_non_g4 = dict()
    for chr in non_g4_intervals:
        non_g4_interval_list = non_g4_intervals[chr]
        for i in tqdm(range(len(non_g4_interval_list))):
            overlaps = False
            g4_interval_list = g4_intervals[chr]
            for j in range(len(g4_interval_list)):
                # if the interval overlaps
                if non_g4_interval_list[i]["interval"].overlaps(g4_interval_list[j]["interval"]):
                    # and orientations are the same then the sequences overlap
                    if g4_interval_list[i]["orientation"] == '.' or non_g4_interval_list[i]["orientation"] == g4_interval_list[j]["orientation"]:
                        overlaps = True
                        break
            if not overlaps:
                if chr not in final_non_overlapping_non_g4:
                    final_non_overlapping_non_g4[chr] = []
                final_non_overlapping_non_g4[chr].append(non_overlapping_non_g4[chr][i])
    return final_non_overlapping_non_g4


def get_kmer(seq_df, k=6):
    """
    Convert sequences into a string of kmer sequences for DNAbert input
    :param seq_df:
    :param k:
    :return:
    """
    seq_list = []
    res_df = seq_df.copy(deep=True)
    for index, row in seq_df.iterrows():
        sequence = row["sequence"]
        kmer_list = []
        for i in range(len(sequence) - k + 1):
            kmer_list.append(sequence[i:i+k])
        seq_list.append(" ".join(kmer_list))
    res_df["sequence"] = seq_list
    return res_df


download_genome_chr_files(version=genome_ver)
get_chr_lengths(version=genome_ver)
convert_genome_version()



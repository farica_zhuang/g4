'''
This file contains functions that downloads gencode gff annotation genome file to obtain sequences of unique utr regions
'''

import pandas as pd
from urllib import request
from tqdm import tqdm
import os
import gzip
from Bio import SeqIO
from Bio.Seq import Seq
import pathlib

from utils import utils


DATA_DIR = os.path.join("/", "data", "g4mer")

BUILD_VERSION = "GRCh38"
HUMAN_GENOME_VERSION = "hg38"
GENCODE_VERSION = "v40"
RELEASE_NUMBER = "40"
REGION = "three_prime_UTR"

def run_all():
    """
    Generate file with sequences from unique utr regions
    :return: N/A
    """

    # obtain all utr regions and they're corresponding sequences from gff annotation file
    # output file: all_[REGION]_regions_[BUILD_VERSION]_[GENCODE_VERSION].csv
    df = extract_all_utr_regions()

    # combine the utr regions per transcript to get transcript sequences
    # output file: all_utr_sequences_[BUILD_VERSION]_[GENCODE_VERSION].csv
    extract_transcript_sequences(df)


def download_annotation_file():
    """
    Download gencode gff annotation file to data folder
    :return: N/A
    """
    class TqdmUpTo(tqdm):
        """
        Progress bar for download
        """
        def update_to(self, b=1, bsize=1, tsize=None):
            """
            b  : int, optional
                Number of blocks transferred so far [default: 1].
            bsize  : int, optional
                Size of each block (in tqdm units) [default: 1].
            tsize  : int, optional
                Total size (in tqdm units). If [default: None] remains unchanged.
            """
            if tsize is not None:
                self.total = tsize
            return self.update(b * bsize - self.n)  # also sets self.n = b * bsize

    out_dir = os.path.join(DATA_DIR, "gencode_files", BUILD_VERSION, GENCODE_VERSION)
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(out_dir, "gencode." + GENCODE_VERSION + ".annotation.gff3.gz")
    # if using GRCh37, use the mapped version using liftover
    if BUILD_VERSION == "GRCh37":
        link = "https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_" + RELEASE_NUMBER + \
               "/GRCh37_mapping/gencode." + GENCODE_VERSION + "lift37.annotation.gff3.gz"
    else:
        link = "https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_" + RELEASE_NUMBER + "/gencode." + GENCODE_VERSION + ".annotation.gff3.gz"

    # download file only if it doesn't exist
    if not os.path.isfile(filename):
        with TqdmUpTo(unit='B', unit_scale=True, unit_divisor=1024, miniters=1,
                      desc=link.split('/')[-1]) as t:  # all optional kwargs
            request.urlretrieve(link, filename=filename,
                                reporthook=t.update_to)
            t.total = t.n
    print("Downloaded annotation file")


def get_sequences_from_coordinates(df):
    """
    Add sequences to the dataframe containing utr regions
    :return: dataframe of utr regions with sequences
    """
    # download chromosome files to get utr sequences
    utils.download_genome_chr_files(version=HUMAN_GENOME_VERSION)

    sequence_dict = dict()
    sequence_list = []
    print("Getting", len(df), "sequences...")
    for index, row in tqdm(df.iterrows()):
        chromosome = row.chromosome
        strand = row.strand
        start = row.start
        end = row.end
        if chromosome not in sequence_dict:
            sequence = get_sequence(chromosome)
            sequence_dict[chromosome] = sequence
        # start -1 because coordinates in gff are 1-based and fully closed
        sequence = sequence_dict[chromosome][start-1:end]
        # if strand is -, get reverse complement of the sequence
        if strand == '-':
            sequence = str(Seq(sequence).reverse_complement().upper())
        sequence_list.append(sequence)
    print("Done getting sequences from coordinates!")
    df['sequence'] = sequence_list
    return df


def get_sequence(chromosome):
    chr_file = os.path.join(DATA_DIR, HUMAN_GENOME_VERSION, chromosome + ".fa.gz")
    # get the current chromosome sequence
    with gzip.open(chr_file, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            sequence = record
    return str(sequence.seq).upper()


def extract_all_utr_regions():
    """
    Create a dataframe with all utr regions from annotation file, output the dataframe
    :return: dataframe of all utr coordinates with their information such as gene name and transcript name
    """
    # download annotation file to get utr coordinates from
    download_annotation_file()

    out_dir = os.path.join(DATA_DIR, "utr", "wildtype")
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(out_dir, "all_" + REGION + "_regions_" + BUILD_VERSION + "_" + GENCODE_VERSION +".csv")
    if not os.path.isfile(filename):
        df = get_all_utr_regions()
        if 'sequence' not in df.columns:
            df = get_sequences_from_coordinates(df)
        df.to_csv(filename, index=None, sep='\t')
        print("Obtained sequences for all " + REGION + " regions")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted all " + REGION + " regions...total regions found:", len(df))
    return df


def extract_transcript_sequences(df):
    """
    Create a dataframe with all utr sequences from dataframe, output the final dataframe
    :return: dataframe with sequences for each utr region
    """
    out_dir = os.path.join(DATA_DIR, "utr", "wildtype")
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(DATA_DIR, "utr", "wildtype", "all_" + REGION + "_sequences_" + BUILD_VERSION + "_" + GENCODE_VERSION + ".csv")
    if not os.path.isfile(filename):
        print("\nExtracting sequences for all transcripts...\n")
        df = get_transcript_sequences(df)
        df.to_csv(filename, index=None, sep='\t')
        print("Obtained sequences for all transcripts")
    else:
        df = pd.read_csv(filename, sep='\t')
    print("Extracted sequences for all transcripts...total sequences found:", len(df))
    return df


def get_transcript_sequences(df):
    """
    Combine utr sequences of transcripts from a dataframe of all utr regions
    :param df:
    :return: dataframe with transcript utr sequences
    """
    df_row_list = []
    # get all the genes in this dataframe
    transcripts = df['transcript_id'].unique()
    for transcript in tqdm(transcripts):
        # find rows in the dataframe with this gene id
        curr_df = df[df['transcript_id'] == transcript]
        # some transcripts appear more than once in different chromosomes, take just the first one
        curr_df = curr_df[curr_df.chromosome == curr_df.chromosome.unique()[0]]
        strand = curr_df.strand.unique()
        assert len(strand) == 1
        strand = strand[0]
        if strand == '-':
            curr_df = curr_df.sort_values(by=['start'], ascending=False)
        elif strand == '+':
            curr_df = curr_df.sort_values(by=['start'], ascending=True)
        sequence = ''
        # concatenate the utr sequences of this transcript
        for index, row in curr_df.iterrows():
            sequence += row.sequence
        df_row_list.append([
            row.chromosome, row.gene_id, row.transcript_id, row.gene_name,
            row.transcript_name, sequence
        ])
    # the number of rows should be equal to the number of transcripts since we combine all the regions belonging to the
    # same transcript
    df = pd.DataFrame(df_row_list, columns=['chromosome', 'gene_id', 'transcript_id', 'gene_name', 'transcript_name',
                                            'sequence'])

    # make sure that the number of transcripts is equal to the number of resulting sequences
    assert len(df['transcript_id'].unique()) == len(df)

    return df


def get_all_utr_regions():
    """
    Extract all utr regions from annotation file
    :return: dataframe consisting of all utr regions
    """
    print("\nGetting all utr regions from annotation file...\n")
    filename = os.path.join(DATA_DIR, "gencode_files", BUILD_VERSION, GENCODE_VERSION,
                            "gencode." + GENCODE_VERSION + ".annotation.gff3.gz")
    annotation_df = pd.read_csv(filename, sep='\t', comment='#', header=None)
    # create a list of dictionaries of the distinct utr regions
    utr_regions_list = []
    for index, row in tqdm(annotation_df.iterrows()):
        region = row[2]
        if region == REGION:
            utr_regions_list.append(extract_utr_regions_info(row))
    print("Retrieved all " + REGION + " regions from annotation file")

    df = pd.DataFrame(utr_regions_list)

    return df


def extract_utr_regions_info(row):
    utr_regions_dict = dict()
    utr_regions_dict['chromosome'] = row[0]
    utr_regions_dict['start'] = row[3]
    utr_regions_dict['end'] = row[4]
    utr_regions_dict['strand'] = row[6]
    gene_info_list = row[8].split(';')
    # want to save gene_id, transcript_id, gene_name, transcript_name
    for info in gene_info_list:
        if "gene_id" in info:
            utr_regions_dict['gene_id'] = info.split('=')[1]
        if "transcript_id" in info:
            utr_regions_dict['transcript_id'] = info.split('=')[1]
        if "gene_name" in info:
            utr_regions_dict['gene_name'] = info.split('=')[1]
        if "transcript_name" in info:
            utr_regions_dict['transcript_name'] = info.split('=')[1]
    return utr_regions_dict


if __name__ == '__main__':
    run_all()

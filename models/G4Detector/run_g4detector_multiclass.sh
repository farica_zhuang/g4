#!/bin/bash

export TRAIN_DATA=/data/g4mer/train_dev/rg4seeker/multiclass/70n/train.tsv
export TEST_DATA=/data/g4mer/train_dev/rg4seeker/multiclass/70n/dev.tsv

python g4_multi_label.py -i $TRAIN_DATA -t $TEST_DATA
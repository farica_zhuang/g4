from preprocessing import generate_negative_strands
import unittest
import pandas as pd


class GenerateNegStrandsTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_seq_dict_no_extension(self):
        # test: without extension
        df = pd.DataFrame([[1, 13, 25], [1, 12, 30], [2, 3, 9]], columns=['chr', 'start', 'end'])
        actual_seq_dict = generate_negative_strands.get_seq_dict(df=df, label="G4", sort=True, sort_by="start")
        expected_seq_dict = {
            1: [{"start": 12, "end": 30, "label": "G4"},
                {"start": 13, "end": 25, "label": "G4"}],
            2: [{"start": 3, "end": 9, "label": "G4"}]
        }
        self.assertEqual(actual_seq_dict, expected_seq_dict)

    def test_get_seq_dict_with_extension(self):
        # test: extending sequences
        df = pd.DataFrame([[1, 13, 17], [1, 12, 15], [2, 3, 9]], columns=['chr', 'start', 'end'])
        actual_seq_dict = generate_negative_strands.get_seq_dict(df=df, label="nonG4", sort=True,
                                                                 sort_by="start", extend=True,
                                                                 extended_length=10)
        expected_seq_dict = {
            1: [{"start": 9, "end": 19, "original_start": 12, "original_end": 15, "label": "nonG4"},
                {"start": 10, "end": 20, "original_start": 13, "original_end": 17, "label": "nonG4"}],
            2: [{"start": 1, "end": 11, "original_start": 3, "original_end": 9, "label": "nonG4"}]
        }
        self.assertEqual(actual_seq_dict, expected_seq_dict)

    def test_get_seq_dict_with_cut(self):
        # test: cut the sequence
        df = pd.DataFrame([[1, 3, 53]], columns=['chr', 'start', 'end'])
        actual_seq_dict = generate_negative_strands.get_seq_dict(df=df, label="nonG4", sort=True,
                                                                 sort_by="start", extend=True,
                                                                 extended_length=10)
        expected_seq_dict = {
            1: [{"start": 23, "end": 33, "original_start": 3, "original_end": 53, "label": "nonG4"}]
        }
        self.assertEqual(actual_seq_dict, expected_seq_dict)

    def test_find_non_overlapping_non_g4(self):
        # test: no overlaps
        g4_seq = {
            1: [{"start": 3, "end": 6, "label": "G4"}]
        }
        nong4_seq = {
            1: [{"start": 9, "end": 19, "original_start": 12, "original_end": 15, "label": "nonG4"}]
        }
        max_chr_len = {1: 100, 2:100}
        actual_res = generate_negative_strands.find_non_overlapping_non_g4(g4_seq, nong4_seq, max_chr_len)
        expected_res = {
            1: [{"start": 9, "end": 19, "original_start": 12, "original_end": 15, "label": "nonG4"}]
        }
        self.assertEqual(actual_res, expected_res)

    def test_find_non_overlapping_non_g4(self):
        # test: shift overlapping g4
        g4_seq = {
            1: [{"start": 8, "end": 11, "label": "G4"}]
        }
        nong4_seq = {
            1: [{"start": 9, "end": 19, "original_start": 12, "original_end": 15, "label": "nonG4"}]
        }
        max_chr_len = {1: 100, 2: 100}
        actual_res = generate_negative_strands.find_non_overlapping_non_g4(g4_seq, nong4_seq, max_chr_len,
                                                                           non_g4_seq_len=10)
        expected_res = {
            1: [{"start": 11, "end": 21, "original_start": 12, "original_end": 15, "label": "nonG4"}]
        }
        self.assertEqual(actual_res, expected_res)

    def test_find_non_overlapping_non_g4(self):
        # test: shifting overlapping g4 gets excluded because original start and end are not included
        g4_seq = {
            1: [{"start": 16, "end": 19, "label": "G4"}]
        }
        nong4_seq = {
            1: [{"start": 9, "end": 19, "original_start": 12, "original_end": 15, "label": "nonG4"}]
        }
        max_chr_len = {1: 100, 2: 100}
        actual_res = generate_negative_strands.find_non_overlapping_non_g4(g4_seq, nong4_seq, max_chr_len,
                                                                           non_g4_seq_len=10)
        expected_res = {1:[]}
        self.assertEqual(actual_res, expected_res)


if __name__ == '__main__':
    unittest.main()

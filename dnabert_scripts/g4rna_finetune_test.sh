#!/bin/bash

# activate dnabert virtual environment
. env_bert/bin/activate

for FOLD in 1 2 3 4 5 6 7 8 9 10
do
  export KMER=6
  export MODEL_PATH=/home/fzhuang/dnabert/DNABERT/examples/output/rg4seeker/binary_train/6mer_70nt_5epochs
  export DATA_PATH=/home/fzhuang/dnabert/DNABERT/examples/cv_data/g4rna/fold_$FOLD
  export OUTPUT_PATH=/home/fzhuang/dnabert/DNABERT/examples/output/cv/finetune_g4rna/fold_$FOLD/

  python /home/fzhuang/dnabert/DNABERT/examples/run_finetune.py \
      --model_type dna \
      --tokenizer_name=dna$KMER \
      --model_name_or_path $MODEL_PATH \
      --task_name dnaprom \
      --do_train \
      --data_dir $DATA_PATH \
      --max_seq_length 70 \
      --per_gpu_eval_batch_size=32   \
      --per_gpu_train_batch_size=32   \
      --learning_rate 2e-4 \
      --num_train_epochs 5.0 \
      --output_dir $OUTPUT_PATH \
      --logging_steps 100 \
      --save_steps 4000 \
      --warmup_percent 0.1 \
      --hidden_dropout_prob 0.1 \
      --overwrite_output \
      --weight_decay 0.01 \
      --n_process 8

  export KMER=6
  export MODEL_PATH=/home/fzhuang/dnabert/DNABERT/examples/output/cv/finetune_g4rna/fold_$FOLD
  export DATA_PATH=/home/fzhuang/dnabert/DNABERT/examples/cv_data/g4rna/fold_$FOLD
  export PREDICTION_PATH=/home/fzhuang/dnabert/DNABERT/examples/output/cv/g4rna/fold_$FOLD
  export PRED_RESULT_OUTPUT_FILE=/home/fzhuang/g4/g4/dnabert_scripts/g4rna_cv/fold_$FOLD

  python /home/fzhuang/dnabert/DNABERT/examples/run_finetune.py \
      --model_type dna \
      --tokenizer_name=dna$KMER \
      --model_name_or_path $MODEL_PATH \
      --task_name dnaprom \
      --do_predict \
      --pred_result_output_file $PRED_RESULT_OUTPUT_FILE \
      --data_dir $DATA_PATH  \
      --max_seq_length 70 \
      --per_gpu_pred_batch_size=128   \
      --output_dir $MODEL_PATH \
      --predict_dir $PREDICTION_PATH \
      --n_process 48

  export DATA_PATH=/home/fzhuang/dnabert/DNABERT/examples/cv_data/rg4seeker/fold_$FOLD
  export PREDICTION_PATH=/home/fzhuang/dnabert/DNABERT/examples/output/cv/rg4seeker/fold_$FOLD
  export PRED_RESULT_OUTPUT_FILE=/home/fzhuang/g4/g4/dnabert_scripts/rg4seeker_cv/fold_$FOLD

  python /home/fzhuang/dnabert/DNABERT/examples/run_finetune.py \
      --model_type dna \
      --tokenizer_name=dna$KMER \
      --model_name_or_path $MODEL_PATH \
      --task_name dnaprom \
      --do_predict \
      --pred_result_output_file $PRED_RESULT_OUTPUT_FILE \
      --data_dir $DATA_PATH  \
      --max_seq_length 70 \
      --per_gpu_pred_batch_size=128   \
      --output_dir $MODEL_PATH \
      --predict_dir $PREDICTION_PATH \
      --n_process 48
done

# deactivate virtual environment
conda deactivate
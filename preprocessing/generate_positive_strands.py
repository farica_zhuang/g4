# Generate active G4 sequences and extend its length to 200bps by mapping to GENCODE transcript fasta file
import pandas as pd
import numpy as np
import pysam
import re 

transcript_file = "/home/diwu/project/G4s/gencode.v39.transcripts.fa"


data = {}
transcript = []



f = pd.read_excel("SupplementaryFile2.xlsx")
for index, row in f.iterrows():
    chrom = row["chromosome"]
    strand = row["strand"]
    intervals = row["Genomic intervals of rG4"].split(',')
    temp_coor = []
    for interval in intervals:
        coors = interval.split(':')[1].split('-')
        temp_coor.append(int(coors[0]))
        temp_coor.append(int(coors[1]))
    start = min(temp_coor)
    end = max(temp_coor)
    seq_diagram = row["Sequence diagram (RTS sites are indicated by asterisks)"]
    seq = seq_diagram.split('\n')[0].split('-')[1].strip()
    if isinstance(row["Overlapping RefSeq \ngene names"], str):
        gene_name = re.split(', | \|', row["Overlapping RefSeq \ngene names"])[0]
    else:
        gene_name = "N/A"
    data[index] = {"chr":chrom, "strand":strand, "start": start, "end": end, "seq": seq, "gene_name": gene_name}
print(f"------------Data Loaded:{len(data)}------------------\n")


with open(transcript_file, 'r') as f1:
    temp = ''
    for line in f1:
        if line[0] == '>':
            if temp != '':
                transcript.append(temp)
            temp = ''
        else:
            temp += line.strip()
    transcript.append(temp)

less_length = 0

with open("data_coordinates.tsv", "w+") as w, open("g4_data",'w+') as w1:
    target_len = 200
    keys = list(data.keys())
    count = 0
    for key in keys:
        item = data[key]
        rg4 = item['seq']
        length = len(rg4)
        extend_len = int((target_len - length)/2)
        if length + 2*extend_len < target_len:
            length += 1
        for seq in transcript:
            index = seq.find(rg4)
            if index != -1:
                count += 1
                if index < extend_len:
                    extended_seq = seq[:target_len]
                elif len(seq) < index + extend_len + length:
                    extended_seq = seq[-target_len:]
                else:
                    extended_seq = seq[index - extend_len : index + extend_len + length]
                if len(extended_seq) != target_len:
                    less_length += 1
                    print(len(extended_seq))

                w.write('\t'.join([item['chr'], item['strand'], str(item['start']), str(item['end'])]) + '\n')
                w1.write(extended_seq + '\n')
                del data[key]
                if count % 100 == 0:
                    print(count)
                break
print(data)
print(less_length)

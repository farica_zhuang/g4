# DNABERT scripts used for G4mer experiments

## Train G4mer
We trained G4mer with all of rg4seeker data so that the resulting G4mer can be used to get predictions of wildtype
and mutated utr sequences

## Test wildtype UTR sequences
Using G4mer trained on all of rg4seker data, get the predicted score of all the wildtype UTR sequences

## Train and test G4mer
We split rg4seeker data into train and dev files so that we can find the best hyperparameters for the optimal G4mer model
trained on rg4seeker data

## G4rna fine tune test
TODO

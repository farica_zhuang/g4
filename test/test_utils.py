import unittest
from preprocessing import utils


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(True, False)  # add assertion here

    def test_check_g4_motif(self):
        # test 1, G4 motif with one middle A
        seq = "GGGAGGG"
        self.assertTrue(utils.check_g4_motif(seq))

    def test_check_g4_motif(self):
        # test 2, G4 motif with 7 N
        seq = "GGGATCGATGGGG"
        self.assertTrue(utils.check_g4_motif(seq))

    def test_check_g4_motif(self):
        # test 3, non G4 motif, no middle nucleotide
        seq = "GGGGGG"
        self.assertFalse(utils.check_g4_motif(seq))

    def test_check_g4_motif(self):
        # test 4, non G4 motif, >7 middle nucleotides
        seq = "GGGACGTACGTGGG"
        self.assertFalse(utils.check_g4_motif(seq))

    def test_get_reverse_complement(self):
        seq = "ATGC"
        actual_result = utils.get_reverse_complement(seq)
        expected_result = "TACG"
        self.assertEqual(actual_result, expected_result)


if __name__ == '__main__':
    unittest.main()

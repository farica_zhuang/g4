#!/bin/bash

# activate dnabert virtual environment
. env_bert/bin/activate

activate
# train g4mer
export MAX_SEQ_LENGTH=70
export EPOCHS=5
export KMER=6
export DNABERT_PATH=/home/fzhuang/dnabert/DNABERT/examples
export MODEL_PATH=$DNABERT_PATH/../pretrained_models/6-new-12w-0
export DATA_PATH=/data/g4mer/rg4seeker/binary/6mer/70n
export OUTPUT_PATH=$DNABERT_PATH/output/rg4seeker/binary/${EPOCHS}_epochs/${MAX_SEQ_LENGTH}n/

mkdir -p ${OUTPUT_PATH}
rm -rf ${OUTPUT_PATH}*

python $DNABERT_PATH/run_finetune.py \
      --model_type dna \
      --tokenizer_name=dna$KMER \
      --model_name_or_path $MODEL_PATH \
      --task_name dnaprom \
      --do_train \
      --data_dir $DATA_PATH \
      --max_seq_length $MAX_SEQ_LENGTH \
      --per_gpu_eval_batch_size=32   \
      --per_gpu_train_batch_size=32   \
      --learning_rate 2e-4 \
      --num_train_epochs $EPOCHS \
      --output_dir $OUTPUT_PATH \
      --logging_steps 100 \
      --save_steps 4000 \
      --warmup_percent 0.1 \
      --hidden_dropout_prob 0.1 \
      --overwrite_output \
      --weight_decay 0.01 \
      --n_process 8

# deactivate virtual environment
conda deactivate

import generate_negative_strands
import utils
import pandas as pd
import os
import random
from sklearn.utils import shuffle
import math
from tqdm import tqdm

data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "utrs")
genome_ver = "hg38"
num_of_non_g4 = 6000
final_num_of_non_g4 = 5000
extended_seq_len = 200
num_per_chr = int(math.floor(num_of_non_g4/24))  # 20 is a buffer

# # read g4 and non g4 sequences
# g4_df = pd.read_csv(os.path.join(data_dir, "g4_coords.hg38.bed"), sep='\t', header=None)
# g4_df["orientation"] = "."
# g4_df.columns = ['chr', 'start', 'end', 'orientation']
# rg4_df = pd.read_csv(os.path.join(data_dir, "rg4seeker_g4_coords.tsv"), sep='\t', header=None)
# rg4_df.columns = ["chr", "orientation", "start", "end"]
# g4_df = pd.concat([g4_df, rg4_df], ignore_index=True).reset_index(drop=True)
# assert len(g4_df.columns) == 4
# non_g4_df = pd.read_csv(os.path.join(data_dir, "non_g4_coords.hg38.bed"), sep='\t', header=None)
# orientation = pd.read_csv(os.path.join(data_dir, "non_g4_coords_with_orientation.hg19.bed"), sep=' ', header=None).iloc[:, 3]
# non_g4_df['orientation'] = orientation
# non_g4_df.columns = ['chr', 'start', 'end', 'orientation']
# # remove unknown chromosomes
# non_g4_df = non_g4_df[non_g4_df["chr"] != "chr15_KI270850v1_alt"]
# non_g4_df = non_g4_df[non_g4_df["chr"] != "chr17_KI270909v1_alt"]
# non_g4_df = non_g4_df[non_g4_df["chr"] != "chr19_KI270938v1_alt"]
# non_g4_df = non_g4_df[non_g4_df["chr"] != "chr22_KI270879v1_alt"]
# non_g4_df = non_g4_df[non_g4_df["chr"] != "chr1_KI270766v1_alt"]
# non_g4_df = non_g4_df[non_g4_df["chr"] != "chr1_KI270765v1_alt"]
# assert len(non_g4_df.columns) == 4
#
# # print stats
# print("The number of g4 sequences is", len(g4_df))
# print("The number of non g4 sequences is", len(non_g4_df))
#
# # get g4 and extended non g4 sequences
# print("Getting G4 and extended non G4 sequences...")
# g4_seq = generate_negative_strands.get_seq_dict(df=g4_df, label="G4", sort=True, sort_by="start")
# non_g4_seq = generate_negative_strands.get_seq_dict(non_g4_df, "nonG4", sort=True, sort_by="start",
#                                                     extend=True, extended_length=extended_seq_len)
#
# # limit the number of non_g4_seq
# for chr in non_g4_seq:
#     non_g4_seq[chr] = non_g4_seq[chr][:num_per_chr]
#
# destination_file = os.path.join("data", genome_ver, "chr_lengths.txt")
# max_chr_len = dict()
# with open(destination_file, 'r') as file:
#     for line in file:
#         chr = line.split("\t")[0]
#         length = line.split("\t")[1]
#         max_chr_len[chr] = length
#
# # find g4 sequences that don't contain g4 sequences in it
# print("Finding non overlapping non G4 sequences...")
# non_overlapping_non_g4 = generate_negative_strands.find_non_overlapping_non_g4(g4_seq, non_g4_seq, max_chr_len,
#                                                      non_g4_seq_len=extended_seq_len)
# # perform sanity check
# print("Performing sanity check.....")
# non_overlapping_non_g4 = utils.check_non_overlapping_non_g4(non_overlapping_non_g4, g4_seq, extended_seq_len=extended_seq_len)
#
# # write extended non g4 sequences to file
# print("Getting sequences from coordinates...")
# non_overlapping_non_g4 = utils.get_sequences_from_coordinates(non_overlapping_non_g4, version=genome_ver, orientation=False, find_g4_motif=False)
# # convert label to 0 and 1 for DNAbert
# non_overlapping_non_g4 = utils.convert_label(non_overlapping_non_g4, neg_string="nonG4", pos_string="G4")
# # collapse the chromosome dictionary into a list of sequence dictionaries
# sequence_list = []
# for chr in non_overlapping_non_g4:
#     sequence_list = sequence_list + non_overlapping_non_g4[chr]
# random.shuffle(sequence_list)
# sequence_list = sequence_list[:final_num_of_non_g4]
# out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "non_g4_data.txt")
# with open(out, 'w') as file:
#     file.write("sequence\tlabel\n")
#     for sequence in sequence_list:
#         file.write("%s\t%s\n" % (sequence["sequence"], sequence["label"]))

# -----------------------------Prepare g4 seq train test data------------------------------
# rg4_df = pd.read_csv(os.path.join(data_dir, "g4_data.txt"), sep='\t', header=None)
# # add label 1 to the sequences
# rg4_df["label"] = 1
# rg4_df.columns = ["sequence", "label"]
# rg4_sequence_list = []
# for index, row in rg4_df.iterrows():
#     rg4_sequence_list.append({
#         "sequence": row["sequence"],
#         "label": row["label"]
#     })
# out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "g4_data.txt")
# with open(out, 'w') as file:
#     file.write("sequence\tlabel\n")
#     for sequence in rg4_sequence_list:
#         file.write("%s\t%s\n" % (sequence["sequence"], sequence["label"]))

# # --------------------------Combine train and test data---------------------------
#
# # get g4 and non g4  data
# g4_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "g4_data.txt"), sep="\t")
# non_g4_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "non_g4_data.txt"), sep="\t")
# seq_df = pd.concat([g4_df, non_g4_df]).reset_index(drop=True)
#
# test_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "test.tsv")
# dev_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "dev.tsv")
# train_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "train.tsv")
#
# seq_list = seq_df.to_dict('records')
# utils.generate_test_train_file(seq_list, test_out=test_out, train_out=train_out, dev_out=dev_out)
#
# # ------------------------------Get the 3, 4, 5, 6 mer data----------------------------
# # k for kmer
# k_list = [3, 4, 5, 6]
#
# # get train, dev, test data
# test_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "test.tsv"), sep="\t")
# train_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "train.tsv"), sep="\t")
# dev_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", "dev.tsv"), sep="\t")
#
# for k in tqdm(k_list):
#     # get the kmer df
#     test_kmer_df = utils.get_kmer(test_df, k=k)
#     train_kmer_df = utils.get_kmer(train_df, k=k)
#     dev_kmer_df = utils.get_kmer(dev_df, k=k)
#
#     # write to file
#     test_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary",
#                              str(k) + "mer", "test.tsv")
#     test_kmer_df.to_csv(test_out, sep="\t", index=False)
#     train_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", str(k) + "mer", "train.tsv")
#     train_kmer_df.to_csv(train_out, sep="\t", index=False)
#     dev_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "binary", str(k) + "mer", "dev.tsv")
#     dev_kmer_df.to_csv(dev_out, sep="\t", index=False)

# ---------------------- Preprocess multiclass data-------------------------------
# get multiclass g4 data
g4_multiclass_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", "g4_data_multi.txt"), sep="\t", comment="#")

dev_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", "dev.tsv")
train_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", "train.tsv")
test_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", "test.tsv")

seq_list = g4_multiclass_df.to_dict('records')
utils.generate_test_train_file(seq_list, test_out=test_out, train_out=train_out, dev_out=dev_out)

# k for kmer
k_list = [3, 4, 5, 6]

# get train, dev, test data
test_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", "test.tsv"), sep="\t")
train_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", "train.tsv"), sep="\t")
dev_df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", "dev.tsv"), sep="\t")

for k in tqdm(k_list):
    # get the kmer df
    test_kmer_df = utils.get_kmer(test_df, k=k)
    train_kmer_df = utils.get_kmer(train_df, k=k)
    dev_kmer_df = utils.get_kmer(dev_df, k=k)

    # write to file
    test_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", str(k) + "mer", "test.tsv")
    test_kmer_df.to_csv(test_out, sep="\t", index=False)
    train_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", str(k) + "mer", "train.tsv")
    train_kmer_df.to_csv(train_out, sep="\t", index=False)
    dev_out = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "train_dev_test", "multiclass", str(k) + "mer", "dev.tsv")
    dev_kmer_df.to_csv(dev_out, sep="\t", index=False)
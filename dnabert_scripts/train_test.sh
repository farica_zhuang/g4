#!/bin/bash

# activate dnabert virtual environment
. env_bert/bin/activate

export MAX_SEQ_LENGTH=80
export EPOCHS=5
export KMER=6
export DNABERT_PATH=/home/fzhuang/dnabert/DNABERT/examples
export DATA_PATH=/home/fzhuang/g4/g4/data
export MODEL_PATH=$DNABERT_PATH/../pretrained_models/6-new-12w-0
export DATA_PATH=$DATA_PATH/train_dev/rg4seeker/binary/${KMER}mer/${MAX_SEQ_LENGTH}n
export OUTPUT_PATH=$DNABERT_PATH/output/rg4seeker/binary_train/${EPOCHS}_epochs/${MAX_SEQ_LENGTH}n/

mkdir -p ${OUTPUT_PATH}
rm -rf ${OUTPUT_PATH}*

python $DNABERT_PATH/run_finetune.py \
      --model_type dna \
      --tokenizer_name=dna$KMER \
      --model_name_or_path $MODEL_PATH \
      --task_name dnaprom \
      --do_train \
      --data_dir $DATA_PATH \
      --max_seq_length $MAX_SEQ_LENGTH \
      --per_gpu_eval_batch_size=32   \
      --per_gpu_train_batch_size=32   \
      --learning_rate 2e-4 \
      --num_train_epochs $EPOCHS \
      --output_dir $OUTPUT_PATH \
      --logging_steps 100 \
      --save_steps 4000 \
      --warmup_percent 0.1 \
      --hidden_dropout_prob 0.1 \
      --overwrite_output \
      --weight_decay 0.01 \
      --n_process 8

export MODEL_PATH=$OUTPUT_PATH
export DATA_PATH=$DATA_PATH
export PREDICTION_PATH=$DNABERT_PATH/output/rg4seeker/binary_test/${EPOCHS}_epochs/${MAX_SEQ_LENGTH}n
export PRED_RESULT_OUTPUT_DIR=/home/fzhuang/g4/g4/dnabert_scripts/output/${EPOCHS}_epochs/${MAX_SEQ_LENGTH}n
export PRED_RESULT_OUTPUT_FILE=${PRED_RESULT_OUTPUT_DIR}/out

mkdir -p ${PREDICTION_PATH}
rm -rf ${PREDICTION_PATH}*

mkdir -p ${PRED_RESULT_OUTPUT_DIR}

python $DNABERT_PATH/run_finetune.py \
    --model_type dna \
    --tokenizer_name=dna$KMER \
    --model_name_or_path $MODEL_PATH \
    --task_name dnaprom \
    --do_predict \
    --pred_result_output_file $PRED_RESULT_OUTPUT_FILE \
    --data_dir $DATA_PATH  \
    --max_seq_length $MAX_SEQ_LENGTH \
    --per_gpu_pred_batch_size=128   \
    --output_dir $MODEL_PATH \
    --predict_dir $PREDICTION_PATH \
    --n_process 48

# deactivate virtual environment
conda deactivate
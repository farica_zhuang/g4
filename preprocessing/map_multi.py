import sys, getopt
import pandas as pd

def main(argv):
    inputfile = ''
    seqfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:s:o:",["ifile=","sfile=","ofile="])
    except getopt.GetoptError:
        print('map_multi.py -i <input label file> -s <seq file> -o <output file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('map_multi.py -i <input label file> -s <seq file> -o <output file>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-s", "--sfile"):
            seqfile = arg
    
    class_label = {}
    label_count = 0
    seq_set = []
    with open(seqfile) as f:
        for line in f:
            seq_set.append(line.strip())

    f = pd.read_excel(inputfile)
    with open(outputfile, 'w+') as w:
        w.write("seq\tlabel")
        for index, row in f.iterrows():
            seq_diagram = row["Sequence diagram (RTS sites are indicated by asterisks)"]
            seq = seq_diagram.split('\n')[0].split('-')[1].strip()
            sc = row["rG4 structural class"].strip()
            for item in seq_set:
                if seq in item:
                    if sc in class_label:
                        label = class_label[sc]
                    else:
                        class_label[sc] = label_count
                        label = label_count
                        label_count += 1
                    w.write(item + '\t' + str(label) + '\n')
                    seq_set.remove(item)
                    break
    print(class_label)
    
if __name__ == "__main__":
    main(sys.argv[1:])
